/*
 * GIP. A Software Manager Tool.
 *
 * Copyright (C) 1999, 2000 :
 * Andreas Hyden <andreas.hyden@telia.com>
 * and
 * Marmorat Irwin <mirwinm@yahoo.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
*/

/////////////////////////////////////////////////////////////////////////////
///////////// softuninstfunc.h - Software Uninstaller Functions /////////////
///////////////////////////////////////////////////////////////////////////// 

#include <gnome.h>

gboolean uninstshown;
gboolean select_radio_on;
gboolean file_selected;
guint uninst_selected_row;
gint selected_files;

/* Declare some variables */
gchar* uInstallDir;
gchar* uInstallFile;
gchar* uTempDir;
gchar* uTempFile;
gchar* uExecFile;
gchar* uFinishFile;
gchar* uSuccessFile;
gint   uInstType;
gpointer uWidgets;
	
enum uWork { prepuninst, prepfile, uninstall, uclean };
	
/* Internal functions used in GIP_init () */
void uSetInstallDir  		(gchar* dir);
void uSetInstallFile 		(gchar* file);
void uSetTempDir	    	(gchar* dir);
void uSetTempFile    		(gchar* file);
void uSetExecFile    		(gchar* file);
void uSetFinishFile	       	(gchar* file);
void uSetSuccessFile		(gchar* file);
void uSetInstType    		(gint insttype);

/* Internal functions */
gboolean uCreateExec 	 	(gint uwork);
gboolean uRunExec	    	(void);
gboolean uCleanUpExec 		(void);

/* Internal Check Status functions */
gboolean uCheckIfInstalled	(void);
gboolean uCheckIfSuccess	(void);

/* Init the GIPInstall structure */
void uGIP_init (gchar* installdir, gchar* tempdir, 
		gchar* tempfile, gchar* execfile,
		gchar* finfile, gchar* succfile,
		gint insttype, gpointer widgets);

/* Public functions used for Installation */
gboolean PrepareUninstallation  (void);
gboolean PrepareFile		(void);
gboolean Uninstall		(void);
gboolean uCleanUp		(void);
void	 uFinish		(void); 	

gboolean
on_Uninstall_activate						(GtkWidget*		widget,
								gpointer		clist);

void
on_SoftwareUninstallCancel_activate				(GtkWidget		*widget,
								gpointer		 window);

gboolean
Destroy_Uninstall						(GtkWidget*		widget,
								 gpointer		user_data);

void
on_uninstall_add_activate					(GtkWidget*		widget,
								 gpointer		clist);

void
on_uninstall_remove_activate					(GtkWidget*		widget,
								 gpointer		clist);

void
on_filesel_cancel_activate 					(GtkWidget*		widget,
								 gpointer		fsel_widget);

void
on_filesel_ok_activate	 					(GtkWidget*		widget,
								 gpointer		fsel_widget);

void
on_ufinished_activate						(GtkWidget		*widget,
								 gpointer		 user_data);
							 			
void
GIP_Uninstall 							(gpointer data);

void
on_uninst_selected_list_activate      		(GtkCList*	  clist,
                                 		 gint             row,
		                 		 gint             column,
		                 		 GdkEvent*	  event,
		                 		 gpointer         widget);

void
on_uninst_unselected_list_activate     		(GtkCList*	  clist,
                                 		 gint             row,
		                 		 gint             column,
		                 		 GdkEvent*	  event,
		                 		 gpointer         widget);
