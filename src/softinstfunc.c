/*
 * GIP. A Software Manager Tool.
 *
 * Copyright (C) 1999, 2000 :
 * Andreas Hyden <andreas.hyden@telia.com>
 * and
 * Marmorat Irwin <mirwinm@yahoo.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
*/

/////////////////////////////////////////////////////////////////////////////
///////////// softinstfunc.c - Software Installer Functions /////////////////
/////////////////////////////////////////////////////////////////////////////

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <string.h>
#include <unistd.h>
#include <time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <errno.h>

#include "softinstfunc.h"
#include "softinsti.h"
#include "mainfunc.h"
#include "maini.h"
#include "support.h"
#include "database.h"
#include "passwdfunc.h"
#include "passwdi.h"
#include "advancedi.h"
#include "advancedfunc.h"
#include "main.h"

DataBase db_entry;

void
SetInstallDir				(gchar* dir)
{
  InstallDir = g_strdup (dir);
}

void
SetInstallFile				(gchar* file)
{
  InstallFile = g_strdup (file);
}

void
SetTempDir				(gchar* dir)
{
  TempDir = g_strdup (dir);
}

void
SetTempFile				(gchar* file)
{
  TempFile = g_strdup (file);
}

void
SetExecFile				(gchar* file)
{
  ExecFile = g_strdup (file);
}

void
SetFinishFile                           (gchar* file)
{
  FinishFile = g_strdup (file);
}

void
SetSuccessFile                          (gchar* file)
{
  SuccessFile = g_strdup (file);
}

void
SetInstType				(gint insttype)
{
  InstType = insttype;
}

gboolean
CreateExec				(gint work)
{
  gint file;
  gint tempfile; 
  gchar* 	script;
  gchar* 	create;
  gchar* 	command_before;
  gchar*	command;
  gchar*        temp = g_strconcat (" ; if [ $? = 0 ]; then\n>> ", SuccessFile, "\nelse\n echo Failed\nfi\n>> ", FinishFile, NULL);
  
  /* Declare and then free the variables because else we get warnings that script etc may not be defined */
  script 		= g_strdup ("h");
  command_before 	= g_strdup ("h");
  create 		= g_strdup ("h");
  command		= g_strdup ("h");
  if (1) {
    g_free (script);
    g_free (command_before);
    g_free (create);
    g_free (command);
  }
  
  UpdateGUI ();
  
  switch (work) {
  case prepare : {
    command_before = g_strdup ("clear");
    command = g_strconcat ("rm -rf ", TempDir, temp, NULL);
    break;
  }
  case copy : {
    command_before = g_strconcat ("mkdir ", TempDir, NULL);
    command = g_strconcat ("cp ", InstallFile, " ", TempDir, temp, NULL);
    break;
  }
  case uncompress : {
    command_before = g_strdup ("clear");
    if (FileType (InstallFile) == src)
      command = g_strconcat ("cd ", TempDir, " ; tar zxvf *gz", temp, NULL);
    else if (FileType (InstallFile) == bz2)
      command = g_strconcat ("cd ", TempDir, " ; bunzip2 *.bz2", " ; tar xvf *.tar", temp, NULL);
    else if (FileType (InstallFile) == tar)
      command = g_strconcat ("cd ", TempDir, " ; tar xvf *.tar", temp, NULL);
    else if (FileType (InstallFile) == tarZ)
      command = g_strconcat ("cd ", TempDir, " ; uncompress *.tar.Z", " ; tar xvf *.tar", temp, NULL);
    else if (FileType (InstallFile) == zip)
      command = g_strconcat ("cd ", TempDir, " ; unzip *.zip", temp, NULL);
    else if (FileType (InstallFile) == rpm)
      command = g_strconcat ("clear", temp, NULL);
    else if (FileType (InstallFile) == deb)
      command = g_strconcat ("clear", temp, NULL);
    break;
  }
  case saveR : {
    command_before = g_strdup ("mkdir ~/local ; mkdir ~/local/doc");
    command = g_strconcat ("`cp -f ", TempDir, "/*/README ", "~/local/doc`", temp, NULL);		
    break;
  }
  case saveF : {
    command_before = g_strdup ("mkdir ~/local ; mkdir ~/local/doc");
    command = g_strconcat ("`cp -f ", TempDir, "/*/FAQ ", "~/local/doc`", temp, NULL);			
    break;
  }
  case configure : {
    command_before = g_strdup ("clear");
    if (AdvancedOptions.ConfigureExtraOptions != NULL)
      command = g_strconcat ("cd ", TempDir, " ; cd `ls -F | grep /` ; sh configure ",
			     AdvancedOptions.ConfigureExtraOptions, temp, NULL);
    else
      command = g_strconcat ("cd ", TempDir, " ; cd `ls -F | grep /` ; sh configure ",
			     temp, NULL);
    break;
  }
  case compile : {
    command_before = g_strdup ("clear");
    if (AdvancedOptions.MakeExtraOptions != NULL)
      command = g_strconcat ("cd ", TempDir, " ; cd `ls -F | grep /` ; make ",
			     AdvancedOptions.MakeExtraOptions, temp, NULL);
    else
      command = g_strconcat ("cd ", TempDir, " ; cd `ls -F | grep /` ; make ",
			     temp, NULL);
    break;
  }
  case global : {
    command_before = g_strdup ("clear");
    if (FileType (InstallFile) == rpm) {
      if (AdvancedOptions.RpmInstallExtraOptions != NULL)
	command = g_strconcat ("rpm -ivh ", AdvancedOptions.RpmInstallExtraOptions
			       , InstallFile, temp, NULL);
      else
	command = g_strconcat ("rpm -ivh ", InstallFile, temp, NULL);
    }
    else if (FileType (InstallFile) == deb) {
      g_print ("Deb installation not yet supported.");
      command = g_strdup (" ");
    }
    else {
      command = g_strconcat ("cd ", TempDir, " ; cd `ls -F | grep /` ; make install", temp, NULL);
    }
    break;
  }
  case clean : {
    command_before = g_strdup ("clear");
    command = g_strconcat ("rm -rf ", TempDir, temp, NULL);
    break;
  }
  }
  
  script = g_strdup (command);
  
  /* Create ExecFile */
  tempfile = open (ExecFile, O_CREAT | O_WRONLY | O_TRUNC);
  close (tempfile);
  
  file = open (ExecFile, O_WRONLY);
  write (file, script, strlen (script));
  close (file);
  
  system (command_before);
  
  g_free (script);
  g_free (command_before);
  g_free (create);
  g_free (command);
  
  UpdateGUI ();
  
  return TRUE;	
}

gboolean
RunExec						(gint work)
{
  clock_t delay = 2 * CLOCKS_PER_SEC;
  clock_t start = clock ();
  gboolean finish;
  gboolean success;
  gchar* execute;
  
  if (work == global && strcmp (g_get_user_name (), "root"))
    CheckRootPassword (pass_status.password);
  else {
    execute = g_strconcat ("bash ", ExecFile, " &", NULL);
    system (execute);
    g_free (execute);
  }	
  
  do {
    while (clock () - start < delay)
      UpdateGUI ();
    finish = CheckIfInstalled ();
    if (!finish) {
      start = clock ();
      continue;
    }
    else
      break;
  } while (1);	
  
  success = CheckIfSuccess ();
  if (!success)
    return FALSE;
  return TRUE;		
}

gboolean
CleanUpExec					(void)
{
  if (g_file_exists (ExecFile))
    remove (ExecFile);
  if (g_file_exists ("/tmp/gip/finish"))
    remove ("/tmp/gip/finish");
  if (g_file_exists ("/tmp/gip/success"))
    remove ("/tmp/gip/success");
  
  return TRUE;
}

gboolean
CheckIfInstalled			(void)
{
  
  if (!g_file_exists ("/tmp/gip/finish"))
    return FALSE;
  remove ("/tmp/gip/finish");
  return TRUE;	
}

gboolean
CheckIfSuccess				(void)
{
  if (!g_file_exists ("/tmp/gip/success"))
    return FALSE;
  remove ("/tmp/gip/success");
  return TRUE;		
}

/* Get info from directory */
gboolean GetDirInfo (const gchar* dir)
{
  gchar* configure;
  gchar* Readme;
  gchar* FAQ;
  
  configure = g_strconcat ("cd ", dir, " ; cd `ls -F | grep /` ; ls configure", NULL);	
  Readme = g_strconcat ("cd ", dir, " ; cd `ls -F | grep /` ; ls README", NULL);
  FAQ = g_strconcat ("cd ", dir, " ; cd `ls -F | grep /` ; ls FAQ", NULL);
  
  UpdateGUI ();
  
  finfo.configure = FALSE;
  finfo.Makefile  = FALSE;
  finfo.Readme    = FALSE;
  finfo.FAQ       = FALSE;
  
  
  if (!system (configure))
    finfo.configure = TRUE;
  if (!system (Readme))
    finfo.Readme = TRUE;
  if (!system (FAQ))
    finfo.FAQ = TRUE;
  finfo.Makefile = TRUE;
  
  UpdateGUI ();
  
  g_free (configure);
  g_free (Readme);
  g_free (FAQ);
  
  return TRUE;
}

void
GIP_init (gchar* installdir, gchar* tempdir,
	  gchar* tempfile, gchar* execfile,
	  gchar* finfile, gchar* succfile,
	  gint insttype, gpointer widgets)
{
  struct stat status;

  /* Set som vaules */
  SetInstallDir	 (installdir);
  SetTempDir	 (tempdir);
  SetTempFile	 (tempfile);
  SetExecFile	 (execfile);
  SetFinishFile  (finfile);
  SetSuccessFile (succfile);
  SetInstType	 (insttype);
  Widgets = widgets;

  if (!stat (InstallDir, &status)) {
    if (!S_ISDIR (status.st_mode)) {
      remove (InstallDir);
      mkdir (InstallDir, S_IRUSR | S_IWUSR | S_IXUSR);
    }
  }
  else if (errno == ENOENT)
    mkdir (InstallDir, S_IRUSR | S_IWUSR | S_IXUSR);

  if (!stat (TempDir, &status)) {
    if (!S_ISDIR (status.st_mode)) {
      remove (TempDir);
      mkdir (TempDir, S_IRUSR | S_IWUSR | S_IXUSR);
    }
    else
      system (g_strconcat ("rm -rf ", TempDir, NULL));
  }
  else if (errno == ENOENT)
    mkdir (TempDir, S_IRUSR | S_IWUSR | S_IXUSR);
  
  // MI
  db_entry = DataBase_InstallEntry_Start(db_entry, InstallDir, 0, 0);
  
  /* Remove some files */
  if (g_file_exists ("/tmp/gip/finish"))
    remove ("/tmp/gip/finish");
  if (g_file_exists ("/tmp/gip/success"))
    remove ("/tmp/gip/success");
  if (g_file_exists ("~/local/doc/FAQ"))
    remove ("~/local/doc/FAQ");
  if (g_file_exists ("~/local/doc/README"))
    remove ("~/local/doc/README");
}

gboolean
PrepareInstallation					(void)
{
  gboolean one   = CreateExec		 	 (prepare);
  gboolean two   = RunExec			 (prepare);
  gboolean three = CleanUpExec		 ();
  
  if (one == FALSE || two == FALSE || three == FALSE)
    return FALSE;
  return TRUE;	
}

gboolean
CopyTempFileToTempDir				(void)
{
  gboolean one   = CreateExec	    	(copy);
  gboolean two   = RunExec	    	(copy);
  gboolean three = CleanUpExec    	();
  
  if (one == FALSE || two == FALSE || three == FALSE)
    return FALSE;
  return TRUE;
}

gboolean
UncompressFile						(void)
{
  gboolean one   = CreateExec 	(uncompress);
  gboolean two   = RunExec		(uncompress);
  gboolean three = CleanUpExec	();
  
  if (one == FALSE || two == FALSE || three == FALSE)
    return FALSE;
  return TRUE;
}

gboolean
SaveReadme						(void)
{
  gboolean one   = CreateExec		(saveR);
  gboolean two   = RunExec		(saveR);
  gboolean three = CleanUpExec		();
  
  if (one == FALSE || two == FALSE || three == FALSE)	
    return FALSE;
  return TRUE;
}

gboolean
SaveFaq							(void)
{
  gboolean one   = CreateExec		(saveF);
  gboolean two   = RunExec		(saveF);
  gboolean three = CleanUpExec		();
  
  if (one == FALSE || two == FALSE || three == FALSE)
    return FALSE;
  return TRUE;
}

gboolean
RunConfigure						(void)
{
  gboolean one   = CreateExec		(configure);
  gboolean two   = RunExec		(configure);
  gboolean three = CleanUpExec	();
  
  if (one == FALSE || two == FALSE || three == FALSE)
    return FALSE;
  return TRUE;
}

gboolean
Compile							(void)
{
  gboolean one   = CreateExec		(compile);
  gboolean two   = RunExec		(compile);
  gboolean three = CleanUpExec	();
  
  if (one == FALSE || two == FALSE || three == FALSE)
    return FALSE;
  return TRUE;
}

gboolean
InstallGlobal						(void)
{
  gboolean one   = CreateExec		(global);
  gboolean two   = RunExec		(global);
  gboolean three = CleanUpExec	();
  
  if (one == FALSE || two == FALSE || three == FALSE)
    return FALSE;
  return TRUE;
}

gboolean
CleanUp							(void)
{
  gboolean one   = CreateExec		(clean);
  gboolean two   = RunExec		(clean);
  gboolean three = CleanUpExec	();
  
  if (one == FALSE || two == FALSE || three == FALSE)
    return FALSE;
  return TRUE;
}

void
Finish							(void)
{
  g_free (InstallDir);
  g_free (InstallFile);
  g_free (TempDir);
  g_free (TempFile);
  g_free (ExecFile);
  g_free (FinishFile);
  g_free (SuccessFile);
}

/* Popups a dialog and ask you if you want to quit the installation */
gboolean 
Cancel_Inst (GnomeDruidPage *page, gpointer arg1, gpointer userdata)
{
  gtk_widget_destroy (GTK_WIDGET (InstallSoftware_window));
  next2shown = FALSE; 				/* Else next2shown will be TRUE and then you will not be able to install again */
  instshown  = FALSE;
  return FALSE;						/* Exit installation */
}

/* Quit installation */
gboolean
Destroy_Install							(GtkWidget*		widget,
								 gpointer		user_data)
{
  instshown = FALSE;
  return FALSE;
}

/* The first next button in druid pressed */
gboolean
on_next1_activate						(GnomeDruidPage *druid_page,
								 gpointer		 arg1,
								 gpointer		 user_data)
{
  gboolean pass_success;
  GtkWidget* mess;
  gchar* file_show = NULL;
  
  /* Get the file the user selected */
  instFile = gnome_file_entry_get_full_path (GNOME_FILE_ENTRY (InstallFile_fileentry), 1);
  
  /* Check if the file the user selected an installation file */
  if (instFile == NULL || FileType (instFile) == non) {
    GtkWidget* mess = gnome_message_box_new (_("You must select an install file (*.tar.gz, *.tgz, *.tar, *.tar.bz2, *.tar.Z, *.zip or *.rpm)."), 																	GNOME_MESSAGE_BOX_WARNING,
					     GNOME_STOCK_BUTTON_OK, NULL);
    gtk_window_set_modal (GTK_WINDOW (mess), TRUE);
    gtk_widget_show (mess);
    return TRUE;
  }
  
  /* Do not continue if the user selected local installation (because it's not implented) */
  if (storeglobal == FALSE) {
    GtkWidget* mess = gnome_message_box_new (_("Sorry. Local installation is not yet implented."), 																	GNOME_MESSAGE_BOX_WARNING,
					     GNOME_STOCK_BUTTON_OK, NULL);
    gtk_window_set_modal (GTK_WINDOW (mess), TRUE);
    gtk_widget_show (mess);
    return TRUE;
  }
  
  
  /* If storeglobal selected and user is not root then popup password dialog */
  else if (storeglobal == TRUE && strcmp (g_get_user_name (), "root")) {
    ShowPasswordDialog ();
    if (pass_status.success == -1) { /* Wrong password */
      mess = gnome_message_box_new (_("Incorrect password!"), GNOME_MESSAGE_BOX_ERROR, GNOME_STOCK_BUTTON_OK, NULL);
      gtk_window_set_modal (GTK_WINDOW (mess), TRUE);
      gtk_widget_show (mess);
      sleep (2);
      return TRUE;
    }
    else if (pass_status.success == -4) /* Cancel pressed */
      return TRUE;
    else if (pass_status.success != -2) { /* We should never get here */
      g_print (_("Error. Check if gip-pass is installed and\nif you got permission to run it.\n"));
      quit_program (-1);
    }
  }
  
  /* Show which packet being installed */
  file_show = g_strconcat (_("Installing Package:"), strrchr (instFile, '/'), NULL);
  file_show[strlen (_("Installing Package:"))] = ' ';
  gtk_label_set_text (GTK_LABEL (Package_label), file_show);
  
  g_free (file_show);
  
  /* Update the GUI */
  UpdateGUI ();	
  
  /* Continue */
  return FALSE;
}

/* Storeglobal radiobutton pressed */
void
on_storeglobal_radiobutton_activate				(GtkWidget		*widget,
								 gpointer		 user_data)
{
  /* Store global */
  storeglobal = TRUE;
}

/* Storelocal radiobutton pressed */
void 
on_storelocal_radiobutton_activate				(GtkWidget		*widget,
								 gpointer		 user_data)
{
  /* Do not store global (store local) */
  storeglobal = FALSE;
}

/* Prepare the preparation of GIP Installation ;) */
void
preparePrepareGipInstall					(GtkWidget		*widget,
								 gpointer		druid)
{
  /* The user have installed software and have pressed back, just disable the back button and return */
  if (next2shown == TRUE)
    {
      gnome_druid_set_buttons_sensitive (GNOME_DRUID (druid), FALSE, TRUE, TRUE);
      return;
    }
  
  /* Disable Back, Next, Cancel and then install */
  gnome_druid_set_buttons_sensitive (GNOME_DRUID (druid), FALSE, FALSE, FALSE);
  GIP_Install (druid);
}

/* Install/Unpgrade Software */
void
GIP_Install							(gpointer		druid)
{
  gboolean notsuccess;
  gchar* homedir;
  gboolean installfailed = FALSE;
  GdkFont* font = gdk_font_load ("fixed");
  float progresstep = 0;
  float progressupdate = 1.0 / 10.0;
  progresstep = progresstep + progressupdate;
  
  /* Get homedirectory */
  homedir = g_strdup (getenv ("HOME"));

  GIP_init (g_strconcat (homedir, "/.gip/temp", NULL), g_strconcat (homedir, "/.gip/temp2", NULL),
	    g_strconcat (homedir, "/.gip/tempfile", NULL), g_strconcat (homedir, "./.gip/temp/exec", NULL),
	    g_strconcat (homedir, "/.gip/temp/finish", NULL), g_strconcat (homedir, "./gip/temp/success", NULL),
	    0, NULL);
  
  /* Source */
  if (FileType (InstallFile) != rpm && FileType (InstallFile) != deb) {
    /* Source code installation (.tar.gz, .tgz and .tar.bz2 etc) */
    gtk_progress_bar_update (GTK_PROGRESS_BAR (InstallStatus_progressbar), progresstep);
    gtk_text_insert (GTK_TEXT (InstallStatus_text), font, NULL, NULL, "Preparing Installation...", -1);
    progresstep = progresstep + progressupdate;	
    
    if (CopyTempFileToTempDir ())
      gtk_text_insert (GTK_TEXT (InstallStatus_text), font, NULL, NULL, "					[    OK    ]\nUncompressing file...", -1);
    else
      gtk_text_insert (GTK_TEXT (InstallStatus_text), font, NULL, NULL, "					[  Failed  ]\nUncompressing file...", -1);	
    
    gtk_progress_bar_update (GTK_PROGRESS_BAR (InstallStatus_progressbar), progresstep);
    progresstep = progresstep + progressupdate;	
    
    if (UncompressFile ())
      gtk_text_insert (GTK_TEXT (InstallStatus_text), font, NULL, NULL, "						[    OK    ]\n", -1);
    else
	 	 	gtk_text_insert (GTK_TEXT (InstallStatus_text), font, NULL, NULL, "						[  Failed  ]\n", -1);
    
    gtk_progress_bar_update (GTK_PROGRESS_BAR (InstallStatus_progressbar), progresstep);
		progresstep = progresstep + progressupdate;
		
		GetDirInfo (TempDir);	

		if (finfo.Readme) {
			gtk_text_insert (GTK_TEXT (InstallStatus_text), font, NULL, NULL, "Saving Readme...", -1);
			if (SaveReadme ())
			  gtk_text_insert (GTK_TEXT (InstallStatus_text), font, NULL, NULL, "							[    OK    ]\n", -1);
			else
			  gtk_text_insert (GTK_TEXT (InstallStatus_text), font, NULL, NULL, "							[  Failed  ]\n", -1);
 		}
	
		gtk_progress_bar_update (GTK_PROGRESS_BAR (InstallStatus_progressbar), progresstep);
		progresstep = progresstep + progressupdate;	

		if (finfo.FAQ) {
		  gtk_text_insert (GTK_TEXT (InstallStatus_text), font, NULL, NULL, "Saving FAQ...", -1);
			if (SaveFaq ())
				gtk_text_insert (GTK_TEXT (InstallStatus_text), font, NULL, NULL, "								[    OK    ]\n", -1);
			else
			  gtk_text_insert (GTK_TEXT (InstallStatus_text), font, NULL, NULL, "								[  Failed  ]\n", -1);
		}	
		
		gtk_progress_bar_update (GTK_PROGRESS_BAR (InstallStatus_progressbar), progresstep);
		progresstep = progresstep + progressupdate;	
		
		if (finfo.configure) {
		  gtk_text_insert (GTK_TEXT (InstallStatus_text), font, NULL, NULL, "Running configure...", -1);
		  if (RunConfigure ())
		    gtk_text_insert (GTK_TEXT (InstallStatus_text), font, NULL, NULL, "						[    OK    ]\n", -1);
		  else
		    gtk_text_insert (GTK_TEXT (InstallStatus_text), font, NULL, NULL, "						[  Failed  ]\n", -1);
		  g_print ("Hejsan");
		}
		
		gtk_progress_bar_update (GTK_PROGRESS_BAR (InstallStatus_progressbar), progresstep);
		progresstep = progresstep + progressupdate;	
		
		GetDirInfo (TempDir);
		
		if (finfo.Makefile) {
		  gtk_text_insert (GTK_TEXT (InstallStatus_text), font, NULL, NULL, "Compiling...", -1);
		  if (Compile ())
		    gtk_text_insert (GTK_TEXT (InstallStatus_text), font, NULL, NULL, "								[    OK    ]\n", -1);
		  else
		    gtk_text_insert (GTK_TEXT (InstallStatus_text), font, NULL, NULL, "								[  Failed  ]\n", -1);

		  gtk_progress_bar_update (GTK_PROGRESS_BAR (InstallStatus_progressbar), progresstep);
			progresstep = progresstep + progressupdate;	
			
			gtk_text_insert (GTK_TEXT (InstallStatus_text), font, NULL, NULL, "Installing Globally...", -1);
			if (InstallGlobal ())
			  gtk_text_insert (GTK_TEXT (InstallStatus_text), font, NULL, NULL, "						[    OK    ]\n", -1);
			else {
			  gtk_text_insert (GTK_TEXT (InstallStatus_text), font, NULL, NULL, "						[  Failed  ]\n", -1);
			  installfailed = TRUE;
			}
		}
		
		gtk_progress_bar_update (GTK_PROGRESS_BAR (InstallStatus_progressbar), progresstep);
		progresstep = progresstep + progressupdate;	

		gtk_text_insert (GTK_TEXT (InstallStatus_text), font, NULL, NULL, "Cleaning Up...", -1);
		if (CleanUp ())
		  gtk_text_insert (GTK_TEXT (InstallStatus_text), font, NULL, NULL, "								[    OK    ]\n\n", -1);
		else
		  gtk_text_insert (GTK_TEXT (InstallStatus_text), font, NULL, NULL, "								[  Failed  ]\n\n", -1);
		
		gtk_progress_bar_update (GTK_PROGRESS_BAR (InstallStatus_progressbar), progresstep);
  }
  else if (FileType (InstallFile) == rpm) {
    gtk_progress_bar_update (GTK_PROGRESS_BAR (InstallStatus_progressbar), (float)0.5);
    gtk_text_insert (GTK_TEXT (InstallStatus_text), font, NULL, NULL, "Installing Rpm-file...", -1);
    
    if (InstallGlobal ())
      gtk_text_insert (GTK_TEXT (InstallStatus_text), font, NULL, NULL, "						[    OK    ]\n\n", -1);
    else {
      gtk_text_insert (GTK_TEXT (InstallStatus_text), font, NULL, NULL, "						[  Failed  ]\n\n", -1);
      installfailed = TRUE;
    }

    gtk_progress_bar_update (GTK_PROGRESS_BAR (InstallStatus_progressbar), (float)1.0);
  }
  
  Finish ();
  
  if (installfailed) {
    gtk_text_insert (GTK_TEXT (InstallStatus_text), font, NULL, NULL, _("Installation Failed!"), -1);
    gnome_druid_set_buttons_sensitive (GNOME_DRUID (druid), FALSE, FALSE, TRUE);
    return;
  }	
  
  /* Installation Complete */
  gtk_progress_bar_update (GTK_PROGRESS_BAR (InstallStatus_progressbar), (float)1.0);
  gtk_text_insert (GTK_TEXT (InstallStatus_text), font, NULL, NULL, _("Installation Successful!\n   Press next to continue..."), -1);
  gnome_druid_set_buttons_sensitive (GNOME_DRUID (druid), FALSE, TRUE, TRUE);
}

void
InstallationFinished						(GnomeDruidPage* druidpage,
								 gpointer		 arg1,
								 gpointer		 window)
{
  gboolean notsuccess;
  
  /* Get some data */
  gboolean runprogram   = gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (RunProgram_checkbutton));
  gboolean viewreadme   = gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (ViewReadme_checkbutton));
  gboolean viewfaq      = gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (ViewFAQ_checkbutton));
  gboolean returnToGip  = gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (ReturnToGip_checkbutton));
  next2shown 	      = FALSE;
  instshown	      = FALSE;
  
  /* Save Installation file */
  if (savefile == TRUE)
    {
      char Temp[300];
      system ("mkdir ~/.gip ; mkdir ~/.gip/progz");
      strcpy (Temp, "cp ");
      strcat (Temp, instFile);
      strcat (Temp, " ~/.gip/progz");
      system (Temp);
    }
  
  /* Run program (not yet implented) */
  if (runprogram == TRUE)
    g_print ("Can not run program\n");
  
  /* View Readme */
  if (viewreadme == TRUE)
    {
      notsuccess = system ("netscape ~/local/doc/README &");
      if (notsuccess)
	system ("gedit ~/local/doc/README &");
    }
  
  /* View FAQ */
  if (viewfaq == TRUE)
    {
      notsuccess = system ("netscape ~/local/doc/FAQ &");
      if (notsuccess)
	system ("gedit ~/local/doc/FAQ &");
    }
  
  //MI
  DataBase_InstallEntry_Finalize(db_entry);
  
  /* Stay or Quit */
  if (returnToGip == TRUE)
    gtk_widget_destroy (GTK_WIDGET (window));
  else
    gtk_main_quit ();
}

/* Saveinstallationfile radiobutton pressed */
void
on_saveinstfile_yes_radiobutton_activate			(GtkWidget		*widget,
								 gpointer		 user_data)
{
  savefile = TRUE;
}

/* Dont saveinstallation radiobutton pressed */
void
on_saveinstfile_no_radiobutton_activate				(GtkWidget		*widget,
								 gpointer		 user_data)
{
  savefile = FALSE;
}
