/*
 * GIP. A Software Manager Tool.
 *
 * Copyright (C) 1999, 2000 :
 * Andreas Hyden <andreas.hyden@telia.com>
 * and
 * Marmorat Irwin <mirwinm@yahoo.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
*/

/////////////////////////////////////////////////////////////////////////////
///////////////////// maini.h - Main Interface //////////////////////////////
/////////////////////////////////////////////////////////////////////////////

#define STARTMAIN 0
#define STARTINSTALL 1
#define STARTUNINSTALL 2

struct mwindow {
	gint xpos, ypos;
	gint height, width;
	gint starttype;
} mwin;

GtkWidget* ReturnToGip_checkbutton;
GtkWidget* Preferences_button;
GtkWidget* Prev_button;
GtkWidget* Close_button;
GtkWidget* GIP_notebook;
gboolean   returnToGip;
gboolean   savefile;
float	   progresstep;
float	   progressupdate;

GtkWidget* MainWindow;

GtkWidget* create_MainWindow 			(void);
GtkWidget* create_About 		       	(void);
















