/*
 * GIP. A Software Manager Tool.
 *
 * Copyright (C) 1999, 2000 :
 * Andreas Hyden <andreas.hyden@telia.com>
 * and
 * Marmorat Irwin <mirwinm@yahoo.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
*/

/////////////////////////////////////////////////////////////////////////////
///////////// advancedi.c - Advanced Interface //////////////////////////////
/////////////////////////////////////////////////////////////////////////////

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <gnome.h>

#include "advancedfunc.h"
#include "advancedi.h"
#include "softinsti.h"
#include "softinstfunc.h"

GtkWidget*
create_AdvancedSrc_dialog (void)
{
  GtkWidget* Advanced_dialog;
  GtkWidget* vbox1;
  GtkWidget* fixed;
  gchar*     pixmap2_filename;
  GtkWidget* pixmap2;
  GtkWidget* label12;
  GtkWidget* label9;
  GtkWidget* frame1;
  GtkWidget* fixed7;
  GtkWidget* label10;
  GtkWidget* entry2;
  GtkWidget* entry1;
  GtkWidget* label11;
  GtkWidget* frame2;
  GtkWidget* fixed8;
  GtkWidget* PopupConf_checkbutton;
  GtkWidget* PopupConfError_checkbutton;
  GtkWidget* PopupMake_checkbutton;
  GtkWidget* PopupMakeError_checkbutton;
  GtkWidget* hbuttonbox1;
  GtkWidget* AdvancedsrcOK_button;
  GtkWidget* AdvancedsrcApply_button;
  GtkWidget* AdvancedsrcCancel_button;

  Advanced_dialog = gtk_window_new (GTK_WINDOW_TOPLEVEL);
  gtk_object_set_data (GTK_OBJECT (Advanced_dialog), "Advanced_dialog", Advanced_dialog);
  gtk_window_set_title (GTK_WINDOW (Advanced_dialog), _("Advanced (Source)"));
  gtk_window_set_policy (GTK_WINDOW (Advanced_dialog), TRUE, FALSE, TRUE);

  vbox1 = gtk_vbox_new (FALSE, 0);
  gtk_container_add (GTK_CONTAINER (Advanced_dialog), vbox1);

  fixed = gtk_fixed_new ();
  gtk_box_pack_start (GTK_BOX (vbox1), fixed, TRUE, TRUE, 0);

  pixmap2 = gtk_type_new (gnome_pixmap_get_type ());
  pixmap2_filename = gnome_pixmap_file ("mc/gnome-package.png");
  if (pixmap2_filename)
    gnome_pixmap_load_file (GNOME_PIXMAP (pixmap2), pixmap2_filename);
  else
    g_warning (_("Couldn't find pixmap file: %s"), "gnome-package.png");
  g_free (pixmap2_filename);
  gtk_fixed_put (GTK_FIXED (fixed), pixmap2, 16, 16);
  gtk_widget_set_uposition (pixmap2, 16, 16);
  gtk_widget_set_usize (pixmap2, 48, 56);

  label12 = gtk_label_new ("");
  gtk_fixed_put (GTK_FIXED (fixed), label12, 328, 336);
  gtk_widget_set_uposition (label12, 328, 336);
  gtk_widget_set_usize (label12, 16, 16);

  label9 = gtk_label_new (_("These settings will be used in this\ninstallation only and will not be saved."));
  gtk_fixed_put (GTK_FIXED (fixed), label9, 96, 24);
  gtk_widget_set_uposition (label9, 96, 24);
  gtk_widget_set_usize (label9, 264, 48);
  gtk_label_set_justify (GTK_LABEL (label9), GTK_JUSTIFY_LEFT);
  gtk_label_set_line_wrap (GTK_LABEL (label9), TRUE);

  frame1 = gtk_frame_new (_("Extra Options"));
  gtk_fixed_put (GTK_FIXED (fixed), frame1, 8, 80);
  gtk_widget_set_uposition (frame1, 8, 80);
  gtk_widget_set_usize (frame1, 352, 120);

  fixed7 = gtk_fixed_new ();
  gtk_container_add (GTK_CONTAINER (frame1), fixed7);

  label10 = gtk_label_new (_("Configure Extra Options:"));
  gtk_fixed_put (GTK_FIXED (fixed7), label10, 8, 8);
  gtk_widget_set_uposition (label10, 8, 8);
  gtk_widget_set_usize (label10, 312, 16);
  gtk_label_set_justify (GTK_LABEL (label10), GTK_JUSTIFY_LEFT);
  gtk_label_set_line_wrap (GTK_LABEL (label10), TRUE);

  entry2 = gnome_entry_new (NULL);
  gtk_fixed_put (GTK_FIXED (fixed7), entry2, 16, 72);
  gtk_widget_set_uposition (entry2, 16, 72);
  gtk_widget_set_usize (entry2, 320, 24);

  MakeExtra_entry = gnome_entry_gtk_entry (GNOME_ENTRY (entry2));
  if (AdvancedOptions.MakeExtraOptions != NULL)
  	gtk_entry_append_text (GTK_ENTRY (MakeExtra_entry), AdvancedOptions.MakeExtraOptions);

  entry1 = gnome_entry_new (NULL);
  gtk_fixed_put (GTK_FIXED (fixed7), entry1, 16, 24);
  gtk_widget_set_uposition (entry1, 16, 24);
  gtk_widget_set_usize (entry1, 320, 24);

  ConfExtra_entry = gnome_entry_gtk_entry (GNOME_ENTRY (entry1));
  if (AdvancedOptions.ConfigureExtraOptions != NULL)
  	gtk_entry_append_text (GTK_ENTRY (ConfExtra_entry), AdvancedOptions.ConfigureExtraOptions);

  label11 = gtk_label_new (_("Make Extra Options:"));
  gtk_fixed_put (GTK_FIXED (fixed7), label11, 8, 56);
  gtk_widget_set_uposition (label11, 8, 56);
  gtk_widget_set_usize (label11, 312, 16);
  gtk_label_set_justify (GTK_LABEL (label11), GTK_JUSTIFY_LEFT);
  gtk_label_set_line_wrap (GTK_LABEL (label11), TRUE);

  frame2 = gtk_frame_new (_("Popup dialogs"));
  gtk_fixed_put (GTK_FIXED (fixed), frame2, 8, 208);
  gtk_widget_set_uposition (frame2, 8, 208);
  gtk_widget_set_usize (frame2, 352, 136);

  fixed8 = gtk_fixed_new ();
  gtk_container_add (GTK_CONTAINER (frame2), fixed8);

  PopupConf_checkbutton = gtk_check_button_new_with_label (_("Popup Configure Extra Options Box"));
  gtk_fixed_put (GTK_FIXED (fixed8), PopupConf_checkbutton, 8, 8);
  gtk_widget_set_uposition (PopupConf_checkbutton, 8, 8);
  gtk_widget_set_usize (PopupConf_checkbutton, 320, 24);
  gtk_widget_set_sensitive (GTK_WIDGET (PopupConf_checkbutton), FALSE);

  PopupConfError_checkbutton = gtk_check_button_new_with_label (_("Popup Configure Extra Options Box on error"));
  gtk_fixed_put (GTK_FIXED (fixed8), PopupConfError_checkbutton, 8, 32);
  gtk_widget_set_uposition (PopupConfError_checkbutton, 8, 32);
  gtk_widget_set_usize (PopupConfError_checkbutton, 320, 24);
  gtk_widget_set_sensitive (GTK_WIDGET (PopupConfError_checkbutton), FALSE);

  PopupMake_checkbutton = gtk_check_button_new_with_label (_("Popup Make Extra Options Box"));
  gtk_fixed_put (GTK_FIXED (fixed8), PopupMake_checkbutton, 8, 64);
  gtk_widget_set_uposition (PopupMake_checkbutton, 8, 64);
  gtk_widget_set_usize (PopupMake_checkbutton, 320, 24);
  gtk_widget_set_sensitive (GTK_WIDGET (PopupMake_checkbutton), FALSE);

  PopupMakeError_checkbutton = gtk_check_button_new_with_label (_("Popup Make Extra Options Box on error"));
  gtk_fixed_put (GTK_FIXED (fixed8), PopupMakeError_checkbutton, 8, 88);
  gtk_widget_set_uposition (PopupMakeError_checkbutton, 8, 88);
  gtk_widget_set_usize (PopupMakeError_checkbutton, 320, 24);
  gtk_widget_set_sensitive (GTK_WIDGET (PopupMakeError_checkbutton), FALSE);

  hbuttonbox1 = gtk_hbutton_box_new ();
  gtk_box_pack_start (GTK_BOX (vbox1), hbuttonbox1, TRUE, TRUE, 0);
  gtk_button_box_set_layout (GTK_BUTTON_BOX (hbuttonbox1), GTK_BUTTONBOX_START);
  gtk_button_box_set_spacing (GTK_BUTTON_BOX (hbuttonbox1), 0);

  AdvancedsrcOK_button = gnome_stock_button (GNOME_STOCK_BUTTON_OK);
  gtk_container_add (GTK_CONTAINER (hbuttonbox1), AdvancedsrcOK_button);
  GTK_WIDGET_SET_FLAGS (AdvancedsrcOK_button, GTK_CAN_DEFAULT);
  gtk_signal_connect (GTK_OBJECT (AdvancedsrcOK_button), "clicked", GTK_SIGNAL_FUNC (on_advancedsrcokbutton_activate), Advanced_dialog);

  AdvancedsrcApply_button = gnome_stock_button (GNOME_STOCK_BUTTON_APPLY);
  gtk_container_add (GTK_CONTAINER (hbuttonbox1), AdvancedsrcApply_button);
  GTK_WIDGET_SET_FLAGS (AdvancedsrcApply_button, GTK_CAN_DEFAULT);
  gtk_widget_set_sensitive (GTK_WIDGET (AdvancedsrcApply_button), FALSE);
  gtk_signal_connect (GTK_OBJECT (AdvancedsrcApply_button), "clicked", GTK_SIGNAL_FUNC (on_advancedsrcapplybutton_activate),
																					AdvancedsrcApply_button);

  AdvancedsrcCancel_button = gnome_stock_button (GNOME_STOCK_BUTTON_CANCEL);
  gtk_container_add (GTK_CONTAINER (hbuttonbox1), AdvancedsrcCancel_button);
  GTK_WIDGET_SET_FLAGS (AdvancedsrcCancel_button, GTK_CAN_DEFAULT);
  gtk_signal_connect (GTK_OBJECT (AdvancedsrcCancel_button), "clicked", GTK_SIGNAL_FUNC (on_advancedcancel_activate), Advanced_dialog);

  gtk_signal_connect (GTK_OBJECT (MakeExtra_entry), "changed", GTK_SIGNAL_FUNC (on_extraentrychanged_activate), AdvancedsrcApply_button);
  gtk_signal_connect (GTK_OBJECT (ConfExtra_entry), "changed", GTK_SIGNAL_FUNC (on_extraentrychanged_activate), AdvancedsrcApply_button);

  gtk_widget_show (vbox1); 
  gtk_widget_show (fixed);
  gtk_widget_show (pixmap2);
  gtk_widget_show (label12);
  gtk_widget_show (label9);
  gtk_widget_show (frame1);
  gtk_widget_show (fixed7);
  gtk_widget_show (label10);
  gtk_widget_show (entry2);
  gtk_widget_show (MakeExtra_entry);
  gtk_widget_show (entry1);
  gtk_widget_show (ConfExtra_entry);
  gtk_widget_show (label11);
  gtk_widget_show (frame2);
  gtk_widget_show (fixed8);
  gtk_widget_show (PopupConf_checkbutton);
  gtk_widget_show (PopupConfError_checkbutton);  
  gtk_widget_show (PopupMake_checkbutton);
  gtk_widget_show (PopupMakeError_checkbutton);
  gtk_widget_show (hbuttonbox1);
  gtk_widget_show (AdvancedsrcOK_button);
  gtk_widget_show (AdvancedsrcApply_button);
  gtk_widget_show (AdvancedsrcCancel_button);

  return Advanced_dialog;
}

GtkWidget*
create_AdvancedRpm_dialog (void)
{
  GtkWidget *AdvancedRpm_dialog;
  GtkWidget *vbox1;
  GtkWidget *fixed;
  GtkWidget *label12;
  gchar *pixmap2_filename;
  GtkWidget *pixmap2;
  GtkWidget *label9;
  GtkWidget *frame1;
  GtkWidget *fixed7;
  GtkWidget *entry2;
  GtkWidget *label11;
  GtkWidget *label10;
  GtkWidget *entry1;
  GtkWidget *frame2;
  GtkWidget *fixed8;
  GtkWidget *PopupRpmInst_checkbutton;
  GtkWidget *PopupRpmUpgrade_checkbutton;
  GtkWidget *hbuttonbox1;
  GtkWidget *AdvancedrpmOK_button;
  GtkWidget *AdvancedrpmApply_button;
  GtkWidget *AdvancedrpmCancel_button;

  AdvancedRpm_dialog = gtk_window_new (GTK_WINDOW_TOPLEVEL);
  gtk_object_set_data (GTK_OBJECT (AdvancedRpm_dialog), "AdvancedRpm_dialog", AdvancedRpm_dialog);
  gtk_window_set_title (GTK_WINDOW (AdvancedRpm_dialog), _("Advanced (Rpm)"));
  gtk_window_set_policy (GTK_WINDOW (AdvancedRpm_dialog), TRUE, FALSE, TRUE);

  vbox1 = gtk_vbox_new (FALSE, 0);
  gtk_widget_show (vbox1);
  gtk_container_add (GTK_CONTAINER (AdvancedRpm_dialog), vbox1);

  fixed = gtk_fixed_new ();
  gtk_widget_show (fixed);
  gtk_box_pack_start (GTK_BOX (vbox1), fixed, TRUE, TRUE, 0);

  label12 = gtk_label_new ("");
  gtk_widget_show (label12);
  gtk_fixed_put (GTK_FIXED (fixed), label12, 352, 280);
  gtk_widget_set_uposition (label12, 352, 280);
  gtk_widget_set_usize (label12, 16, 16);

  pixmap2 = gtk_type_new (gnome_pixmap_get_type ());
  pixmap2_filename = gnome_pixmap_file ("mc/gnome-pack-rpm.png");
  if (pixmap2_filename)
    gnome_pixmap_load_file (GNOME_PIXMAP (pixmap2), pixmap2_filename);
  else
    g_warning (_("Couldn't find pixmap file: %s"), "mc/gnome-pack-rpm.png");
  g_free (pixmap2_filename);
  gtk_widget_show (pixmap2);
  gtk_fixed_put (GTK_FIXED (fixed), pixmap2, 16, 16);
  gtk_widget_set_uposition (pixmap2, 16, 16);
  gtk_widget_set_usize (pixmap2, 48, 56);

  label9 = gtk_label_new (_("These settings will be used in this\ninstallation only and will not be saved."));
  gtk_widget_show (label9);
  gtk_fixed_put (GTK_FIXED (fixed), label9, 96, 24);
  gtk_widget_set_uposition (label9, 96, 24);
  gtk_widget_set_usize (label9, 264, 48);
  gtk_label_set_justify (GTK_LABEL (label9), GTK_JUSTIFY_LEFT);
  gtk_label_set_line_wrap (GTK_LABEL (label9), TRUE);

  frame1 = gtk_frame_new (_("Extra Options"));
  gtk_widget_show (frame1);
  gtk_fixed_put (GTK_FIXED (fixed), frame1, 8, 80);
  gtk_widget_set_uposition (frame1, 8, 80);
  gtk_widget_set_usize (frame1, 352, 120);

  fixed7 = gtk_fixed_new ();
  gtk_widget_show (fixed7);
  gtk_container_add (GTK_CONTAINER (frame1), fixed7);

  entry2 = gnome_entry_new (NULL);
  gtk_widget_show (entry2);
  gtk_fixed_put (GTK_FIXED (fixed7), entry2, 16, 72);
  gtk_widget_set_uposition (entry2, 16, 72);
  gtk_widget_set_usize (entry2, 320, 24);

  RpmUpgradeExtra_entry = gnome_entry_gtk_entry (GNOME_ENTRY (entry2));
  gtk_widget_show (RpmUpgradeExtra_entry);
  if (AdvancedOptions.RpmUpgradeExtraOptions != NULL)
  	gtk_entry_append_text (GTK_ENTRY (RpmUpgradeExtra_entry), AdvancedOptions.RpmUpgradeExtraOptions);
  gtk_widget_set_sensitive (GTK_WIDGET (RpmUpgradeExtra_entry), FALSE);

  label11 = gtk_label_new (_("Rpm Upgrade Extra Options:"));
  gtk_widget_show (label11);
  gtk_fixed_put (GTK_FIXED (fixed7), label11, 8, 56);
  gtk_widget_set_uposition (label11, 8, 56);
  gtk_widget_set_usize (label11, 312, 16);
  gtk_label_set_justify (GTK_LABEL (label11), GTK_JUSTIFY_LEFT);
  gtk_label_set_line_wrap (GTK_LABEL (label11), TRUE);

  label10 = gtk_label_new (_("Rpm Install Extra Options:"));
  gtk_widget_show (label10);
  gtk_fixed_put (GTK_FIXED (fixed7), label10, 8, 8);
  gtk_widget_set_uposition (label10, 8, 8);
  gtk_widget_set_usize (label10, 312, 16);
  gtk_label_set_justify (GTK_LABEL (label10), GTK_JUSTIFY_LEFT);
  gtk_label_set_line_wrap (GTK_LABEL (label10), TRUE);

  entry1 = gnome_entry_new (NULL);
  gtk_widget_show (entry1);
  gtk_fixed_put (GTK_FIXED (fixed7), entry1, 16, 24);
  gtk_widget_set_uposition (entry1, 16, 24);
  gtk_widget_set_usize (entry1, 320, 24);

  RpmInstExtra_entry = gnome_entry_gtk_entry (GNOME_ENTRY (entry1));
  gtk_widget_show (RpmInstExtra_entry);
  if (AdvancedOptions.RpmInstallExtraOptions != NULL)
  	gtk_entry_append_text (GTK_ENTRY (RpmInstExtra_entry), AdvancedOptions.RpmInstallExtraOptions);

  frame2 = gtk_frame_new (_("Popup dialogs"));
  gtk_widget_show (frame2);
  gtk_fixed_put (GTK_FIXED (fixed), frame2, 8, 208);
  gtk_widget_set_uposition (frame2, 8, 208);
  gtk_widget_set_usize (frame2, 352, 80);

  fixed8 = gtk_fixed_new ();
  gtk_widget_show (fixed8);
  gtk_container_add (GTK_CONTAINER (frame2), fixed8);
  gtk_widget_set_usize (fixed8, -2, 56);

  PopupRpmInst_checkbutton = gtk_check_button_new_with_label (_("Popup Rpm Install Extra Options on error"));
  gtk_widget_show (PopupRpmInst_checkbutton);
  gtk_fixed_put (GTK_FIXED (fixed8), PopupRpmInst_checkbutton, 8, 8);
  gtk_widget_set_uposition (PopupRpmInst_checkbutton, 8, 8);
  gtk_widget_set_usize (PopupRpmInst_checkbutton, 328, 24);
  gtk_widget_set_sensitive (GTK_WIDGET (PopupRpmInst_checkbutton), FALSE);

  PopupRpmUpgrade_checkbutton = gtk_check_button_new_with_label (_("Popup Rpm Upgrade Extra Options on error"));
  gtk_widget_show (PopupRpmUpgrade_checkbutton);
  gtk_fixed_put (GTK_FIXED (fixed8), PopupRpmUpgrade_checkbutton, 8, 32);
  gtk_widget_set_uposition (PopupRpmUpgrade_checkbutton, 8, 32);
  gtk_widget_set_usize (PopupRpmUpgrade_checkbutton, 328, 24);
  gtk_widget_set_sensitive (GTK_WIDGET (PopupRpmUpgrade_checkbutton), FALSE);

  hbuttonbox1 = gtk_hbutton_box_new ();
  gtk_widget_show (hbuttonbox1);
  gtk_box_pack_start (GTK_BOX (vbox1), hbuttonbox1, TRUE, TRUE, 0);
  gtk_button_box_set_layout (GTK_BUTTON_BOX (hbuttonbox1), GTK_BUTTONBOX_START);
  gtk_button_box_set_spacing (GTK_BUTTON_BOX (hbuttonbox1), 0);

  AdvancedrpmOK_button = gnome_stock_button (GNOME_STOCK_BUTTON_OK);
  gtk_widget_show (AdvancedrpmOK_button);
  gtk_container_add (GTK_CONTAINER (hbuttonbox1), AdvancedrpmOK_button);
  GTK_WIDGET_SET_FLAGS (AdvancedrpmOK_button, GTK_CAN_DEFAULT);
  gtk_signal_connect (GTK_OBJECT (AdvancedrpmOK_button), "clicked", GTK_SIGNAL_FUNC (on_advancedrpmokbutton_activate),
  													AdvancedRpm_dialog);

  AdvancedrpmApply_button = gnome_stock_button (GNOME_STOCK_BUTTON_APPLY);
  gtk_widget_show (AdvancedrpmApply_button);
  gtk_container_add (GTK_CONTAINER (hbuttonbox1), AdvancedrpmApply_button);
  GTK_WIDGET_SET_FLAGS (AdvancedrpmApply_button, GTK_CAN_DEFAULT);
  gtk_signal_connect (GTK_OBJECT (AdvancedrpmApply_button), "clicked", GTK_SIGNAL_FUNC (on_advancedrpmapplybutton_activate),
  													AdvancedrpmApply_button);

  AdvancedrpmCancel_button = gnome_stock_button (GNOME_STOCK_BUTTON_CANCEL);
  gtk_widget_show (AdvancedrpmCancel_button);
  gtk_container_add (GTK_CONTAINER (hbuttonbox1), AdvancedrpmCancel_button);
  GTK_WIDGET_SET_FLAGS (AdvancedrpmCancel_button, GTK_CAN_DEFAULT);
  gtk_signal_connect (GTK_OBJECT (AdvancedrpmCancel_button), "clicked", GTK_SIGNAL_FUNC (on_advancedcancel_activate), AdvancedRpm_dialog);

  gtk_signal_connect (GTK_OBJECT (RpmInstExtra_entry), "changed", GTK_SIGNAL_FUNC (on_extraentrychanged_activate), AdvancedrpmApply_button);
  gtk_signal_connect (GTK_OBJECT (RpmUpgradeExtra_entry), "changed", GTK_SIGNAL_FUNC (on_extraentrychanged_activate), AdvancedrpmApply_button);

  return AdvancedRpm_dialog;
}
