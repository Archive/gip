/*
 * GIP. A Software Manager Tool.
 *
 * Copyright (C) 1999, 2000 :
 * Andreas Hyden <andreas.hyden@telia.com>
 * and
 * Marmorat Irwin <mirwinm@yahoo.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
*/
 
/////////////////////////////////////////////////////////////////////////////
///////////// softuninsti.c - Software Uninstaller Interface ////////////////
/////////////////////////////////////////////////////////////////////////////

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include "softuninsti.h"
#include "softuninstfunc.h"
#include "maini.h"
#include "support.h"
#include "database.h"

extern guint ListSelected_selected_row;

GtkWidget*
create_Uninstall_window (void)
{
  GtkWidget* Uninstall_window;
  GtkWidget* vbox1;
  GtkWidget* fixed1;
  GtkWidget* viewport1;
  gchar* pixmap1_filename;
  GtkWidget* pixmap1;
  GtkWidget* fixed2;
  GtkWidget* label6;
  GtkWidget* hseparator1;
  GtkWidget* Space_label;
  gchar* tempfile[1];

  selected_files = 0;
  
  Uninstall_window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
  gtk_object_set_data (GTK_OBJECT (Uninstall_window), "Uninstall_window", Uninstall_window);
  gtk_window_set_title (GTK_WINDOW (Uninstall_window), _("Uninstall Software"));
  gtk_window_set_policy (GTK_WINDOW (Uninstall_window), FALSE, TRUE, TRUE);
  gtk_signal_connect (GTK_OBJECT (Uninstall_window), "destroy", GTK_SIGNAL_FUNC (Destroy_Uninstall), NULL);

  vbox1 = gtk_vbox_new (FALSE, 0);
  gtk_widget_show (vbox1);
  gtk_container_add (GTK_CONTAINER (Uninstall_window), vbox1);

  fixed1 = gtk_fixed_new ();
  gtk_widget_show (fixed1);
  gtk_box_pack_start (GTK_BOX (vbox1), fixed1, TRUE, TRUE, 0);

  viewport1 = gtk_viewport_new (NULL, NULL);
  gtk_widget_show (viewport1);
  gtk_fixed_put (GTK_FIXED (fixed1), viewport1, 8, 8);
  gtk_widget_set_uposition (viewport1, 8, 8);
  gtk_widget_set_usize (viewport1, 96, 352);

  pixmap1 = gtk_type_new (gnome_pixmap_get_type ());
  pixmap1_filename = gnome_pixmap_file ("gip/gip-uninstall.png");
  if (pixmap1_filename)
    gnome_pixmap_load_file (GNOME_PIXMAP (pixmap1), pixmap1_filename);
  else
    g_warning (_("Couldn't find pixmap file: %s"), "gip-uninstall.png");
  g_free (pixmap1_filename);
  gtk_widget_show (pixmap1);
  gtk_container_add (GTK_CONTAINER (viewport1), pixmap1);

  Uninstall_frame = gtk_frame_new (_("Files"));
  gtk_widget_show (Uninstall_frame);
  gtk_fixed_put (GTK_FIXED (fixed1), Uninstall_frame, 112, 8);
  gtk_widget_set_uposition (Uninstall_frame, 112, 8);
  gtk_widget_set_usize (Uninstall_frame, 280, 168);

  fixed2 = gtk_fixed_new ();
  gtk_widget_show (fixed2);
  gtk_container_add (GTK_CONTAINER (Uninstall_frame), fixed2);

  PackageUninstall_clist = gtk_clist_new (1);
  gtk_widget_show (PackageUninstall_clist);
  gtk_fixed_put (GTK_FIXED (fixed2), PackageUninstall_clist, 8, 0);
  gtk_widget_set_uposition (PackageUninstall_clist, 8, 0);
  gtk_widget_set_usize (PackageUninstall_clist, 256, 112);
  gtk_clist_set_column_width (GTK_CLIST (PackageUninstall_clist), 0, 80);
  gtk_clist_column_titles_show (GTK_CLIST (PackageUninstall_clist));

  label6 = gtk_label_new (_("Packages to Uninstall"));
  gtk_widget_show (label6);
  gtk_clist_set_column_widget (GTK_CLIST (PackageUninstall_clist), 0, label6);

  UninstallAdd_button = gtk_button_new_with_label (_("Add..."));
  gtk_widget_show (UninstallAdd_button);
  gtk_fixed_put (GTK_FIXED (fixed2), UninstallAdd_button, 8, 120);
  gtk_widget_set_uposition (UninstallAdd_button, 8, 120);
  gtk_widget_set_usize (UninstallAdd_button, 72, 24);
  gtk_signal_connect (GTK_OBJECT (UninstallAdd_button), "clicked", GTK_SIGNAL_FUNC (on_uninstall_add_activate), PackageUninstall_clist);

  UninstallRemove_button = gtk_button_new_with_label (_("Remove"));
  gtk_widget_show (UninstallRemove_button);
  gtk_fixed_put (GTK_FIXED (fixed2), UninstallRemove_button, 88, 120);
  gtk_widget_set_uposition (UninstallRemove_button, 88, 120);
  gtk_widget_set_usize (UninstallRemove_button, 72, 24);
  gtk_widget_set_sensitive (GTK_WIDGET (UninstallRemove_button), FALSE);
  gtk_signal_connect (GTK_OBJECT (UninstallRemove_button), "clicked", GTK_SIGNAL_FUNC (on_uninstall_remove_activate), PackageUninstall_clist);

  gtk_signal_connect (GTK_OBJECT (PackageUninstall_clist), "select_row", GTK_SIGNAL_FUNC (on_uninst_selected_list_activate), UninstallRemove_button);
  gtk_signal_connect (GTK_OBJECT (PackageUninstall_clist), "unselect_row", GTK_SIGNAL_FUNC (on_uninst_unselected_list_activate), UninstallRemove_button);

  UninstallPackage_progressbar = gtk_progress_bar_new ();
  gtk_widget_show (UninstallPackage_progressbar);
  gtk_fixed_put (GTK_FIXED (fixed1), UninstallPackage_progressbar, 240, 192);
  gtk_widget_set_uposition (UninstallPackage_progressbar, 240, 192);
  gtk_widget_set_usize (UninstallPackage_progressbar, 144, 16);

  UninstallTotal_progressbar = gtk_progress_bar_new ();
  gtk_widget_show (UninstallTotal_progressbar);
  gtk_fixed_put (GTK_FIXED (fixed1), UninstallTotal_progressbar, 240, 224);
  gtk_widget_set_uposition (UninstallTotal_progressbar, 240, 224);
  gtk_widget_set_usize (UninstallTotal_progressbar, 144, 16);

  UninstallPackage_label = gtk_label_new (_("Uninstalling Package\n"));
  gtk_widget_show (UninstallPackage_label);
  gtk_fixed_put (GTK_FIXED (fixed1), UninstallPackage_label, 112, 192);
  gtk_widget_set_uposition (UninstallPackage_label, 112, 192);
  gtk_widget_set_usize (UninstallPackage_label, 120, 16);
  gtk_label_set_justify (GTK_LABEL (UninstallPackage_label), GTK_JUSTIFY_LEFT);
  gtk_label_set_line_wrap (GTK_LABEL (UninstallPackage_label), TRUE);
  gtk_widget_set_sensitive (GTK_WIDGET (UninstallPackage_label), FALSE);

  UninstallTotal_label = gtk_label_new (_("Total:"));
  gtk_widget_show (UninstallTotal_label);
  gtk_fixed_put (GTK_FIXED (fixed1), UninstallTotal_label, 112, 224);
  gtk_widget_set_uposition (UninstallTotal_label, 112, 224);
  gtk_widget_set_usize (UninstallTotal_label, 120, 16);
  gtk_label_set_justify (GTK_LABEL (UninstallTotal_label), GTK_JUSTIFY_LEFT);
  gtk_label_set_line_wrap (GTK_LABEL (UninstallTotal_label), TRUE);
  gtk_widget_set_sensitive (GTK_WIDGET (UninstallTotal_label), FALSE);

  Uninstall_button = gtk_button_new_with_label (_("Uninstall"));
  gtk_widget_show (Uninstall_button);
  gtk_fixed_put (GTK_FIXED (fixed1), Uninstall_button, 112, 335);
  gtk_widget_set_uposition (Uninstall_button, 112, 335);
  gtk_widget_set_usize (Uninstall_button, 96, 24);
  gtk_signal_connect (GTK_OBJECT (Uninstall_button), "clicked", GTK_SIGNAL_FUNC (on_Uninstall_activate), PackageUninstall_clist);

  UninstallMessage_label = gtk_label_new (_("Select packages to Uninstall."));
  gtk_widget_show (UninstallMessage_label);
  gtk_fixed_put (GTK_FIXED (fixed1), UninstallMessage_label, 112, 272);
  gtk_widget_set_uposition (UninstallMessage_label, 112, 272);
  gtk_widget_set_usize (UninstallMessage_label, 280, 40);
  gtk_label_set_justify (GTK_LABEL (UninstallMessage_label), GTK_JUSTIFY_LEFT);
  gtk_label_set_line_wrap (GTK_LABEL (UninstallMessage_label), TRUE);

  hseparator1 = gtk_hseparator_new ();
  gtk_widget_show (hseparator1);
  gtk_fixed_put (GTK_FIXED (fixed1), hseparator1, 112, 256);
  gtk_widget_set_uposition (hseparator1, 112, 256);
  gtk_widget_set_usize (hseparator1, 280, 16);

  UninstallCancel_button = gnome_stock_button (GNOME_STOCK_BUTTON_CANCEL);
  gtk_widget_show (UninstallCancel_button);
  gtk_fixed_put (GTK_FIXED (fixed1), UninstallCancel_button, 216, 335);
  gtk_widget_set_uposition (UninstallCancel_button, 216, 335);
  gtk_widget_set_usize (UninstallCancel_button, 96, 24);
  gtk_signal_connect (GTK_OBJECT (UninstallCancel_button), "clicked", GTK_SIGNAL_FUNC (on_SoftwareUninstallCancel_activate), Uninstall_window);

  Space_label = gtk_label_new ("");
  gtk_widget_show (Space_label);
  gtk_fixed_put (GTK_FIXED (fixed1), Space_label, 384, 352);
  gtk_widget_set_uposition (Space_label, 384, 352);
  gtk_widget_set_usize (Space_label, 16, 16);

  return Uninstall_window;
}

GtkWidget*
create_fileselection1 ()
{
  GtkWidget *fileselection1;
  GtkWidget *ok_button1;
  GtkWidget *cancel_button1;

  fileselection1 = gtk_file_selection_new (_("Select File"));
  gtk_object_set_data (GTK_OBJECT (fileselection1), "fileselection1", fileselection1);
  gtk_container_set_border_width (GTK_CONTAINER (fileselection1), 10);

  ok_button1 = GTK_FILE_SELECTION (fileselection1)->ok_button;
  gtk_object_set_data (GTK_OBJECT (fileselection1), "ok_button1", ok_button1);
  gtk_widget_show (ok_button1);
  GTK_WIDGET_SET_FLAGS (ok_button1, GTK_CAN_DEFAULT);
  gtk_signal_connect (GTK_OBJECT (ok_button1), "clicked", GTK_SIGNAL_FUNC (on_filesel_ok_activate), fileselection1);

  cancel_button1 = GTK_FILE_SELECTION (fileselection1)->cancel_button;
  gtk_object_set_data (GTK_OBJECT (fileselection1), "cancel_button1", cancel_button1);
  gtk_widget_show (cancel_button1);
  GTK_WIDGET_SET_FLAGS (cancel_button1, GTK_CAN_DEFAULT);
  gtk_signal_connect (GTK_OBJECT (cancel_button1), "clicked", GTK_SIGNAL_FUNC (on_filesel_cancel_activate), fileselection1);

  return fileselection1;
}