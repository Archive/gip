/*
 * GIP. A Software Manager Tool.
 *
 * Copyright (C) 1999, 2000 :
 * Andreas Hyden <andreas.hyden@telia.com>
 * and
 * Marmorat Irwin <mirwinm@yahoo.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
*/

#include <gnome.h>
#include <time.h>
#include <glib.h>

#define GNOME_APPS_DIR "/usr/share/gnome/apps"

#define GIP_DB_INSTALL_FILE    "DataBase_Install"
#define GIP_DB_UNINSTALL_FILE  "DataBase_Uninstall"

#define GIP_DB_INSTALL_ID   0
#define GIP_DB_UNINSTALL_ID 1
#define GIP_DB_GNOME_ID     2

#define GIP_DB_VERSION1 '1'

// type 
#define GIP_TYPE_UNKNOWN 0
#define GIP_TYPE_GNOME   1
#define GIP_TYPE_KDE     2

//format
#define NB_FORMAT 11 
#define GIP_FORMAT_UNKNOWN 12 


typedef struct TDate Date;
struct TDate
{
  char hour;
  char min;
  
  char mon;
  char day;
  char year;
};

typedef struct TDataBase * DataBase;
struct TDataBase
{
  gchar * name;
  gchar * version;
  gchar * path;
  gchar * filename;
  gchar * comment;
  gchar * app_type; //application type like "game" or "utilitaires"
  gchar * author;
  gchar * exec;

  char type;    // for further use
  char option;  // for further use
  char format;
  long size;
  
  char has_gip_spec_file;
  
  Date time;
  GSList * desktop_files;
  GSList * directory_files;
  DataBase next;
};

typedef struct TDb_file Db_File;
struct TDb_file
{
  gchar  * filename;
  GSList * db;
};

// initialize database
void
DataBase_Init();

// read database from file database_file (sets by database_init())
void
DataBase_ReadAll();

// installentry used only when user install new software
DataBase
DataBase_InstallEntry_Start(DataBase item,char *filename,int type,int option);

void
DataBase_InstallEntry_Finalize(DataBase item);

void
DataBase_UninstallEntry(int item);

// save database !!!
void
DataBase_SaveAll();

void
DataBase_Close();

// update view list 
void
DataBase_UpdateViewList(int id,GtkWidget * list);

// other functions
void
DataBase_UnregisterSoftware(char * section, char * appname);

void
DataBase_RegisterSoftware(DataBase dbentry);

void
DataBase_AddEntry(int id,char *filename,
                         char *name,
			 char *path,
			 char *version,
			 char *comment,
			 char *app_type,
			 char *author,
			 char *exec,
			 int type,
                         int option,
			 int format,
			 long size,
			 int hour,
			 int min,
			 int mon,
			 int day,
			 int year);

void 
DataBase_Read(int id);
void
DataBase_SaveAll();

void 
DataBase_SaveItem(DataBase item,FILE * fdb);

void
DataBase_Save(int id);

void 
DataBase_Install_View_AddItem(DataBase item,gpointer user_data);

void 
DataBase_Uninstall_View_AddItem(DataBase item,gpointer user_data);

void
DataBase_UpdateAllView();

int
DataBase_Util_GetFormat(char * string);

void
DataBase_Util_GetInfo(char * soft,char ** name,char ** path,char ** version,int * format);

void
DataBase_Util_Show_Info(guint item);

gchar *
DataBase_Get_Name_From_Entry_Num(guint item);

void
DataBase_RegisterSoftware_With_DeskopFile(gchar * desktop_filename,DataBase dbentry);

void
DataBase_RegisterSoftware_Write_Directory_File(gchar * dir,DataBase dbentry);

void
DataBase_RegisterSoftware_Write_Default_Desktop_File(gchar * entry,DataBase db_entry);

void
DataBase_RegisterSoftware_Default(DataBase dbentry);

void
DataBase_RegisterSoftware(DataBase dbentry);

