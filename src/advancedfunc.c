/*
 * GIP. A Software Manager Tool.
 *
 * Copyright (C) 1999, 2000 :
 * Andreas Hyden <andreas.hyden@telia.com>
 * and
 * Marmorat Irwin <mirwinm@yahoo.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
*/

/////////////////////////////////////////////////////////////////////////////
///////////// advancedfunc.c - Advanced Function ////////////////////////////
/////////////////////////////////////////////////////////////////////////////

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <gnome.h>

#include "advancedfunc.h"
#include "advancedi.h"
#include "softinsti.h"
#include "softinstfunc.h"
#include "mainfunc.h"

void
on_advancedbutton_activate					(GtkWidget*		widget,
									 gpointer		user_data)
{
	GtkWidget* advanced;
	gchar* file = gnome_file_entry_get_full_path (GNOME_FILE_ENTRY (InstallFile_fileentry), 1);
	
	/* Check if the file the user selected an installation file */
	if (file == NULL || FileType (file) == non || FileType (file) == deb) {
		GtkWidget* mess = gnome_message_box_new (_("You must select an install file (*.tar.gz, *.tgz, *.tar, *.tar.bz2, *.tar.Z, *.zip or *.rpm)."), 																	GNOME_MESSAGE_BOX_WARNING,
			GNOME_STOCK_BUTTON_OK, NULL);
		gtk_window_set_modal (GTK_WINDOW (mess), TRUE);
		gtk_widget_show (mess);
		return;
	}
	if (FileType (file) != rpm && FileType (file) != deb)
		advanced = create_AdvancedSrc_dialog ();
	else if (FileType (file) == rpm)
		advanced = create_AdvancedRpm_dialog ();
		
	gtk_widget_show (advanced);
}

void
on_installfilentrychanged_activate			(GtkWidget*		widget,
									 gpointer 		fileentry)
{
	if (!strcmp (gtk_entry_get_text (fileentry), ""))
		gtk_widget_set_sensitive (GTK_WIDGET (Advanced_button), FALSE);
	else	
		gtk_widget_set_sensitive (GTK_WIDGET (Advanced_button), TRUE);
}

void
on_advancedcancel_activate					(GtkWidget*		widget,
									 gpointer		window)
{
	gtk_widget_destroy (GTK_WIDGET (window));
}

void
on_advancedsrcokbutton_activate				(GtkWidget*		widget,
									 gpointer		window)
{
	AdvancedOptions.ConfigureExtraOptions = g_strdup (gtk_entry_get_text (GTK_ENTRY (ConfExtra_entry)));
	AdvancedOptions.MakeExtraOptions      = g_strdup (gtk_entry_get_text (GTK_ENTRY (MakeExtra_entry)));
	gtk_widget_destroy (GTK_WIDGET (window));
}

void
on_advancedrpmapplybutton_activate			(GtkWidget*		widget,
									 gpointer		apply_button)
{
	AdvancedOptions.RpmInstallExtraOptions = g_strdup (gtk_entry_get_text (GTK_ENTRY (RpmInstExtra_entry)));
	AdvancedOptions.RpmUpgradeExtraOptions = g_strdup (gtk_entry_get_text (GTK_ENTRY (RpmUpgradeExtra_entry)));
	gtk_widget_set_sensitive (GTK_WIDGET (apply_button), FALSE);
}

void
on_advancedrpmokbutton_activate				(GtkWidget*		widget,
									 gpointer		window)
{
	AdvancedOptions.RpmInstallExtraOptions = g_strdup (gtk_entry_get_text (GTK_ENTRY (RpmInstExtra_entry)));
	AdvancedOptions.RpmUpgradeExtraOptions = g_strdup (gtk_entry_get_text (GTK_ENTRY (RpmUpgradeExtra_entry)));
	gtk_widget_destroy (GTK_WIDGET (window));
}

void
on_advancedsrcapplybutton_activate			(GtkWidget*		widget,
									 gpointer		apply_button)
{
	AdvancedOptions.ConfigureExtraOptions = g_strdup (gtk_entry_get_text (GTK_ENTRY (ConfExtra_entry)));
	AdvancedOptions.MakeExtraOptions      = g_strdup (gtk_entry_get_text (GTK_ENTRY (MakeExtra_entry)));
	gtk_widget_set_sensitive (GTK_WIDGET (apply_button), FALSE);
}

void
on_extraentrychanged_activate				(GtkWidget*		widget,
									 gpointer		apply_button)
{
	gtk_widget_set_sensitive (GTK_WIDGET (apply_button), TRUE);
}