/*
 * GIP. A Software Manager Tool.
 *
 * Copyright (C) 1999, 2000 :
 * Andreas Hyden <andreas.hyden@telia.com>
 * and
 * Marmorat Irwin <mirwinm@yahoo.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
*/
 
/////////////////////////////////////////////////////////////////////////////
///////////// softuninsti.h - Software Uninstaller Interface ////////////////
/////////////////////////////////////////////////////////////////////////////

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <gnome.h>

GtkWidget* UninstallFile_fileentry;
GtkWidget* Uninstall_label;
GtkWidget* Uninstall_button;
GtkWidget* UninstallCancel_button;
GtkWidget* UninstallPackage_progressbar;
GtkWidget* UninstallTotal_progressbar;
GtkWidget* PackageUninstall_clist;
GtkWidget* UninstallMessage_label;
GtkWidget* UninstallPackage_label;
GtkWidget* UninstallTotal_label;
GtkWidget* UninstallAdd_button;
GtkWidget* UninstallRemove_button;
GtkWidget* Uninstall_frame;

GtkWidget*
create_Uninstall_window (void);

GtkWidget*
create_fileselection1 (void);