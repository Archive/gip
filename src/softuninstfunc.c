/*
 * GIP. A Software Manager Tool.
 *
 * Copyright (C) 1999, 2000 :
 * Andreas Hyden <andreas.hyden@telia.com>
 * and
 * Marmorat Irwin <mirwinm@yahoo.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
*/
 
/////////////////////////////////////////////////////////////////////////////
///////////// softuninstfunc.c - Software Uninstaller Functions /////////////
/////////////////////////////////////////////////////////////////////////////

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include "softuninstfunc.h"
#include "softuninsti.h"
#include "mainfunc.h"
#include "maini.h"
#include "support.h"
#include "database.h"

#include <string.h>
#include <unistd.h>
#include <time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <errno.h>

extern guint ListSelected_selected_row;

void
uSetInstallDir				(gchar* dir)
{
  uInstallDir = g_strdup (dir);
}

void
uSetInstallFile				(gchar* file)
{
  uInstallFile = g_strdup (file);
}

void
uSetTempDir				(gchar* dir)
{
  uTempDir = g_strdup (dir);
}

void
uSetTempFile				(gchar* file)
{
  uTempFile = g_strdup (file);
}

void
uSetFinishFile				(gchar* file)
{
  uFinishFile = g_strdup (file);
}

void
uSetSuccessFile				(gchar* file)
{
  uSuccessFile = g_strdup (file);
}

void
uSetExecFile				(gchar* file)
{
  uExecFile = g_strdup (file);
}

void
uSetInstType				(gint insttype)
{
  uInstType = insttype;
}

gboolean
uCreateExec				(gint uWork)
{
  gint file;
  gint tempfile;
  gchar* 	script;
  gchar* 	create;
  gchar* 	command_before;
  gchar*        command;
  gchar*        temp = g_strconcat (" ; if [ $? = 0 ]; then\n>> ", uSuccessFile, "\nelse\n echo Failed\nfi\n>> ", uFinishFile, NULL);
  
  /* Declare and then free the variables because else we get warnings that the vars may not be defined */
  script             = g_strdup ("h");
  create             = g_strdup ("h");
  command_before     = g_strdup ("h");
  command            = g_strdup ("h");
  if (1) {
    g_free (script);
    g_free (create);
    g_free (command_before);
    g_free (command);
  }
  
  switch (uWork) {
  case prepuninst : {	
    command_before = g_strconcat ("clear");
    command = g_strconcat ("cp ", uInstallFile, " ", uTempDir, temp, NULL);
    break;
  }
  case prepfile : {
    command_before = g_strdup ("clear");
    
    if (FileType (uInstallFile) == src)
      command = g_strconcat ("cd ", uTempDir, " ; tar zxvf *gz", temp, NULL); 
    else if (FileType (uInstallFile) == bz2)
      command = g_strconcat ("cd ", uTempDir, " ; bunzip2 *.bz2", " ; tar xvf *.tar", temp, NULL);
    else if (FileType (uInstallFile) == tar)
      command = g_strconcat ("cd ", uTempDir, " ; tar xvf *.tar", temp, NULL);
    else if (FileType (uInstallFile) == tarZ)
      command = g_strconcat ("cd ", uTempDir, " ; uncompress *.tar.Z", " ; tar xvf *.tar", temp, NULL);
    else if (FileType (uInstallFile) == zip)
      command = g_strconcat ("cd ", uTempDir, " ; unzip *.zip", temp, NULL);
    else if (FileType (uInstallFile) == rpm)
      command = g_strconcat ("clear", temp, NULL);
    else if (FileType (uInstallFile) == deb)
      command = g_strconcat ("clear", temp, NULL);
    break;
  }
  case uninstall : {
    command_before = g_strdup ("clear");
    
    if (FileType (uInstallFile) == rpm)
      command = g_strconcat ("rpm -e ", uInstallFile, temp, NULL);
    else if (FileType (uInstallFile) == deb) {
      g_print ("Deb uninstallation not yet supported.");
      command = g_strdup (" ");
    }		
    else {
      command = g_strconcat ("cd ", uTempDir, " ; cd `ls -F | grep /` ; sh configure", " ; make uninstall", temp, NULL);
    }
    break;
  }
  case uclean : {
    command_before = g_strdup ("clear");
    command = g_strconcat ("rm -rf ", uTempDir, temp, NULL);
    break;
  }
  }
  
  script = g_strdup (command);
  
  /* Create uExecFile */
  tempfile = open (uExecFile, O_CREAT | O_WRONLY | O_TRUNC);
  close (tempfile);
  
  file = open (uExecFile, O_WRONLY | O_CREAT);
  write (file, script, strlen (script));
  close (file);
  
  system (command_before);
  
  g_free (script);
  g_free (create);
  g_free (command_before);
  g_free (command);
  
  return TRUE;	
}

gboolean
uRunExec					(void)
{
  clock_t   delay = 2 * CLOCKS_PER_SEC;
  clock_t   start = clock ();
  gboolean  finish;
  gboolean  success;
  gchar*    execute;
  
  execute = g_strconcat ("bash ", uExecFile, " &", NULL);
  system (execute);
  g_free (execute); 
  
  do {
    while (clock () - start < delay)
      UpdateGUI ();
    finish = uCheckIfInstalled ();
    if (!finish) {
      start = clock ();
      continue;
    }
    else
      break;
  } while (1);	
  
  success = uCheckIfSuccess ();
  if (!success)
    return FALSE;
  return TRUE;		
}

gboolean
uCleanUpExec					(void)
{
  if (g_file_exists (uExecFile))
    remove (uExecFile);
  if (g_file_exists (uFinishFile))
    remove (uFinishFile);
  if (g_file_exists (uSuccessFile))
    remove (uSuccessFile);
  
  return TRUE;	
}

gboolean
uCheckIfInstalled			(void)
{
  if (!g_file_exists (uFinishFile))
    return FALSE;
  remove (uFinishFile);
  return TRUE;		
}

gboolean
uCheckIfSuccess				(void)
{
  if (!g_file_exists (uSuccessFile))
    return FALSE;
  remove (uSuccessFile);
  return TRUE;		
}

void uGIP_init (gchar* installdir, gchar* tempdir,
		gchar* tempfile, gchar* execfile,
		gchar* finfile, gchar* succfile, 
		gint insttype, gpointer widgets)
{
  struct stat status;

  g_return_if_fail (installdir != NULL);
  g_return_if_fail (tempdir != NULL);
  g_return_if_fail (execfile != NULL);
  g_return_if_fail (finfile != NULL);
  g_return_if_fail (succfile != NULL);

  /* Set som vaules */
  uSetInstallDir 	(installdir);
  uSetTempDir    	(tempdir);
  uSetTempFile	        (tempfile);
  uSetExecFile	        (execfile);
  uSetFinishFile	(finfile);
  uSetSuccessFile       (succfile);
  uSetInstType	        (insttype);
  uWidgets = widgets;

  if (!stat (uInstallDir, &status)) {
    if (!S_ISDIR (status.st_mode)) {
      remove (uInstallDir);
      mkdir (uInstallDir, S_IRUSR | S_IWUSR | S_IXUSR);
    }
  }
  else if (errno == ENOENT)
    mkdir (uInstallDir, S_IRUSR | S_IWUSR | S_IXUSR);
  
  if (!stat (uTempDir, &status)) {
    if (!S_ISDIR (status.st_mode)) {
      remove (uTempDir);
      mkdir (uTempDir, S_IRUSR | S_IWUSR | S_IXUSR);
    }
    else
      system (g_strconcat ("rm -rf ", uTempDir, NULL));
  }
  else if (errno == ENOENT)
    mkdir (uTempDir, S_IRUSR | S_IWUSR | S_IXUSR);

  /* Remove some files */
  if (g_file_exists (uFinishFile))
    remove (uFinishFile);
  if (g_file_exists (uSuccessFile))
    remove (uSuccessFile);
}

gboolean
PrepareUninstallation					(void)
{
  gboolean one   = uCreateExec	 	 (prepuninst);
  gboolean two   = uRunExec	    	 ();
  gboolean three = uCleanUpExec		 ();
  
  if (one == FALSE || two == FALSE || three == FALSE)
    return FALSE;
  return TRUE;	
}

gboolean
PrepareFile						(void)
{
  gboolean one   = uCreateExec	    	(prepfile);
  gboolean two   = uRunExec	    	();
  gboolean three = uCleanUpExec    	();
  
  if (one == FALSE || two == FALSE || three == FALSE)
    return FALSE;
  return TRUE;
}

gboolean
Uninstall						(void)
{
  gboolean one   = uCreateExec 		(uninstall);
  gboolean two   = uRunExec		();
  gboolean three = uCleanUpExec	();
  
  if (one == FALSE || two == FALSE || three == FALSE)
    return FALSE;
  return TRUE;
}

gboolean
uCleanUp						(void)
{
  gboolean one   = uCreateExec		(uclean);
  gboolean two   = uRunExec		();
  gboolean three = uCleanUpExec		();
  
  if (one == FALSE || two == FALSE || three == FALSE)	
    return FALSE;
  return TRUE;
}

void
uFinish							(void)
{
  g_free (uInstallDir);
  g_free (uInstallFile);
  g_free (uTempDir);
  g_free (uTempFile);
  g_free (uExecFile);
  g_free (uFinishFile);
  g_free (uSuccessFile);
}

gboolean
on_Uninstall_activate					(GtkWidget*		widget,
							 gpointer		clist)
{
  gchar* file;
  int quit;
  
  /* Check if there is any files in the list */
  if (file_selected != TRUE) {
    GtkWidget* mess = gnome_message_box_new (_("You must select an install file (*.tar.gz, *.tgz, *.tar, *.tar.bz2, *.tar.Z, *.zip or *.rpm)."), 																GNOME_MESSAGE_BOX_WARNING,
					     GNOME_STOCK_BUTTON_OK, NULL);
    gtk_window_set_modal (GTK_WINDOW (mess), TRUE);
    gtk_widget_show (mess);
    return TRUE;
  }
  
  /* Get the first file name */
  gtk_clist_get_text (GTK_CLIST (clist), 0, 0, &file);
  
  /* Check if it's a valid filename */
  if (FileType (file) == non) {
    GtkWidget* mess = gnome_message_box_new (_("You must select an install file (*.tar.gz, *.tgz, *.tar, *.tar.bz2, *.tar.Z, *.zip or *.rpm)."), 																	GNOME_MESSAGE_BOX_WARNING,
					     GNOME_STOCK_BUTTON_OK, NULL);
    gtk_window_set_modal (GTK_WINDOW (mess), TRUE);
    gtk_widget_show (mess);
    return TRUE;
  }
  
  /* Set Install File */
  uSetInstallFile (file);	
  
  /* Disable/enable som widgets while installing */
  gtk_widget_set_sensitive (GTK_WIDGET (Uninstall_button), FALSE);
  gtk_widget_set_sensitive (GTK_WIDGET (UninstallCancel_button), FALSE);
  gtk_widget_set_sensitive (GTK_WIDGET (UninstallAdd_button), FALSE);
  gtk_widget_set_sensitive (GTK_WIDGET (UninstallRemove_button), FALSE);
  gtk_widget_set_sensitive (GTK_WIDGET (PackageUninstall_clist), FALSE);
  gtk_widget_set_sensitive (GTK_WIDGET (Uninstall_frame), FALSE);
  gtk_widget_set_sensitive (GTK_WIDGET (UninstallPackage_label), TRUE);
  gtk_widget_set_sensitive (GTK_WIDGET (UninstallTotal_label), TRUE);
  
  gtk_label_set_text (GTK_LABEL (UninstallMessage_label), "Uninstalling selected packages...");
  
  /* Start Uninstallation */
  GIP_Uninstall (clist);
  
  /* Continue */
  return TRUE;
}

void
on_SoftwareUninstallCancel_activate				(GtkWidget		*widget,
								 gpointer		 window)
{
  uninstshown = FALSE;
  gtk_widget_destroy (window);
}

gboolean
Destroy_Uninstall						(GtkWidget*		widget,
								 gpointer		user_data)
{
  uninstshown = FALSE;
  return FALSE;
}

void
GIP_Uninstall 							(gpointer data)
{
  gchar* file;
  gboolean notsuccess;
  GtkWidget* mess;
  gchar* homedir;
  gboolean uninstallfailed = FALSE;
  float progresstep = 0;
  float progressteptotal = 0;
  float progressupdate = 1.0 / 5.0;
  float progressupdatetotal = 1.0 / selected_files;
  
  /* Number of selected files */
  selected_files -= 1;
  
  /* Get homedirectory */
  homedir = g_strdup (getenv ("HOME"));
  
  while (selected_files >= 0) {     
    uGIP_init (g_strconcat (homedir, "/.gip/temp", NULL), g_strconcat (homedir, "/.gip/temp2", NULL),
	       g_strconcat (homedir, "/.gip/tempfile", NULL), g_strconcat (homedir, "/.gip/temp/exec", NULL),
	       g_strconcat (homedir, "/.gip/temp/finish", NULL), g_strconcat (homedir, "/.gip/temp/success", NULL),
	       0, NULL);
    
    gtk_clist_get_text (GTK_CLIST (data), selected_files, 0, &file);
    uSetInstallFile (file);
    
    if (FileType (uInstallFile) != rpm && FileType (uInstallFile) != deb) {
      
      gtk_progress_bar_update (GTK_PROGRESS_BAR (UninstallTotal_progressbar), progressteptotal);
      progressteptotal = progressteptotal + progressupdatetotal;
      progresstep = progresstep + progressupdate;
      gtk_progress_bar_update (GTK_PROGRESS_BAR (UninstallPackage_progressbar), progresstep);
      
      PrepareUninstallation ();
      
      progresstep = progresstep + progressupdate;
      gtk_progress_bar_update (GTK_PROGRESS_BAR (UninstallPackage_progressbar), progresstep);	
      
      PrepareFile ();
      
      progresstep = progresstep + progressupdate;
      gtk_progress_bar_update (GTK_PROGRESS_BAR (UninstallPackage_progressbar), progresstep);	
      
      if (!Uninstall ())
	uninstallfailed = TRUE;  	
      
      progresstep = progresstep + progressupdate;
      gtk_progress_bar_update (GTK_PROGRESS_BAR (UninstallPackage_progressbar), progresstep);	
      
      uCleanUp ();
      
      progresstep = progresstep + progressupdate;
      gtk_progress_bar_update (GTK_PROGRESS_BAR (UninstallPackage_progressbar), progresstep);
    }
    else if (FileType (uInstallFile) == rpm) {
      gtk_progress_bar_update (GTK_PROGRESS_BAR (UninstallPackage_progressbar), (float)0.5);
      gtk_progress_bar_update (GTK_PROGRESS_BAR (UninstallTotal_progressbar), (float)0.5);
      
      if (!Uninstall ()) 
	uninstallfailed = TRUE;
      
      gtk_progress_bar_update (GTK_PROGRESS_BAR (UninstallPackage_progressbar), (float)1.0);
      gtk_progress_bar_update (GTK_PROGRESS_BAR (UninstallTotal_progressbar), (float)1.0);
    }
    
    gtk_clist_set_text (data, selected_files, 0, "");
    
    /* Update variables */
    selected_files -= 1;
    progresstep = 0;
    
    /* Free memory */
    uFinish ();
  }
  gtk_progress_bar_update (GTK_PROGRESS_BAR (UninstallTotal_progressbar), (float)1.0);
  
  if (uninstallfailed) {
    mess = gnome_message_box_new (_("Uninstallation Failed!"), GNOME_MESSAGE_BOX_WARNING, GNOME_STOCK_BUTTON_OK, NULL);
    gtk_window_set_modal (GTK_WINDOW (mess), TRUE);
    gtk_label_set_text (GTK_LABEL (UninstallMessage_label), _("Uninstallation Failed."));
    gtk_widget_set_sensitive (GTK_WIDGET (UninstallCancel_button), TRUE);
    gtk_widget_show (mess);
    return;
  }	
  
  /* Uninstallation Complete */
  gtk_progress_bar_update (GTK_PROGRESS_BAR (UninstallPackage_progressbar), (float)1.0);
  mess = gnome_message_box_new ("Uninstallation Successful!", GNOME_MESSAGE_BOX_INFO, GNOME_STOCK_BUTTON_OK, NULL);
  gtk_window_set_modal (GTK_WINDOW (mess), TRUE);
  gtk_label_set_text (GTK_LABEL (UninstallMessage_label), _("Uninstallation Successful."));
  gtk_widget_set_sensitive (GTK_WIDGET (UninstallCancel_button), TRUE);
  gtk_widget_show (mess);
  
  //MI
  DataBase_UninstallEntry (ListSelected_selected_row);
}

void
on_uninstall_add_activate					(GtkWidget*		widget,
								 gpointer		clist)
{
  GtkWidget* Uninstall_fileselection;
  Uninstall_fileselection = create_fileselection1 ();
  gtk_widget_show (Uninstall_fileselection);	
}

void
on_uninstall_remove_activate					(GtkWidget*		widget,
								 gpointer		clist)
{
  if (uninst_selected_row	== -1)
    return;
  gtk_clist_remove (clist, uninst_selected_row);
  selected_files -= 1;
  file_selected = FALSE;
}

void
on_filesel_cancel_activate 					(GtkWidget*		widget,
								 gpointer		fsel_widget)
{
  gtk_widget_destroy (GTK_WIDGET (fsel_widget));
}

void
on_filesel_ok_activate	 					(GtkWidget*		widget,
								 gpointer		fsel_widget)
{
  gchar* temp = gtk_file_selection_get_filename (GTK_FILE_SELECTION  (fsel_widget));
  gchar* filename[1];
  filename[0] = g_strdup (temp);
  
  if (temp[strlen(temp)-1] == '/') {
    GtkWidget* mess = gnome_message_box_new (_("You must select at least one file."), GNOME_MESSAGE_BOX_WARNING, GNOME_STOCK_BUTTON_OK, NULL);
    gtk_window_set_modal (GTK_WINDOW (mess), TRUE);
    gtk_widget_show (mess);
    return;
  }
  
  gtk_clist_append (GTK_CLIST (PackageUninstall_clist), filename);
  file_selected = TRUE;
  selected_files += 1;
  gtk_label_set_text (GTK_LABEL (UninstallMessage_label), _("Press Uninstall to uninstall the selected\npackages."));
  g_free (filename[0]);
  gtk_widget_destroy (fsel_widget);	
}

void
on_uninst_selected_list_activate      		(GtkCList*	  clist,
                                 		 gint             row,
		                 		 gint             column,
		                 		 GdkEvent*	  event,
		                 		 gpointer         widget)
{                                      	
  uninst_selected_row = row;
  gtk_widget_set_sensitive (GTK_WIDGET (widget), TRUE);
}

void
on_uninst_unselected_list_activate     		(GtkCList*	  clist,
                                 		 gint             row,
		                 		 gint             column,
		                 		 GdkEvent*	  event,
		                 		 gpointer         widget)
{
  uninst_selected_row = -1;
  gtk_widget_set_sensitive (GTK_WIDGET (widget), FALSE);
}
