/*
 * GIP. A Software Manager Tool.
 *
 * Copyright (C) 1999, 2000 :
 * Andreas Hyden <andreas.hyden@telia.com>
 * and
 * Marmorat Irwin <mirwinm@yahoo.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
*/

/////////////////////////////////////////////////////////////////////////////
///////////// convertfunc.c - Convert Functions /////////////////////////////
/////////////////////////////////////////////////////////////////////////////

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include "convertfunc.h"
#include "converti.h"
#include "mainfunc.h"
#include "maini.h"
#include "support.h"

void
on_Convert_activate				(GtkWidget	*widget,
						 gpointer	 user_data)
{
	guint chars;
	char Temp[300];
	char *convertfile;
	gboolean notsuccess;
	char* combo_string;
	
	/* Get the file the user selected */
	convertfile = gnome_file_entry_get_full_path (GNOME_FILE_ENTRY (ConvertFile_fileentry), 1);

	/* Check if the user selected a file */
	if (convertfile == NULL)		// no file selected
	{
	
		GtkWidget* mess = gnome_message_box_new (_("You must select a file first."), GNOME_MESSAGE_BOX_WARNING,
			GNOME_STOCK_BUTTON_OK, NULL);
		gtk_widget_show (mess);
		return;
	}

	/* Check what kind of file the user selected */
	if (FileType (convertfile) == non)
	{
		GtkWidget* mess = gnome_message_box_new (_("You must select one of the following files: .tar.gz, .tgz, .rpm or .deb."), 																	GNOME_MESSAGE_BOX_WARNING,
			GNOME_STOCK_BUTTON_OK, NULL);
		gtk_widget_show (mess);
		return;
	}
	
	/* Show some status */
	chars = gtk_text_get_length (GTK_TEXT (ConvertStatus_text));
	gtk_editable_delete_text (GTK_EDITABLE (ConvertStatus_text), 0, chars);
	gtk_text_insert (GTK_TEXT (ConvertStatus_text), NULL, NULL, NULL, "Preparing...", -1);
	combo_string = gtk_entry_get_text (GTK_ENTRY (Convert_combo_entry));
	
	if (!strcmp (combo_string, "Debian (.deb)"))
		strcpy (Temp, "alien -d ");
	else if (!strcmp (combo_string, "Redhat (.rpm)"))
		strcpy (Temp, "alien -r ");
	else
		strcpy (Temp, "alien -t ");
		
	strcat (Temp, convertfile);
	gtk_text_insert (GTK_TEXT (ConvertStatus_text), NULL, NULL, NULL, "OK\nConverting file...", -1);	
	
	/* Excecute */
	notsuccess = system (Temp);
	
	convertshown = FALSE;
	
	/* Not successful */
	if (notsuccess)
	{
		gtk_text_insert (GTK_TEXT (ConvertStatus_text), NULL, NULL, NULL, "Failed\n\nConversion Failed, Aborted.", -1);
		return;
	}
	else
		/* Successful */
		gtk_text_insert (GTK_TEXT (ConvertStatus_text), NULL, NULL, NULL, "OK\n\nConversion Finished.", -1);			
}

void
on_ConvertClose_activate				(GtkWidget		*widget,
								 gpointer		 user_data)
{
	/* Close Convert dialog */

	convertshown = FALSE;
	gtk_widget_destroy (GTK_WIDGET (Convert_window));	
}

gboolean
Destroy_convert						(GtkWidget*		widget,
							 gpointer		user_data)
{
	/* Close Convert dialog */
	
	convertshown = FALSE;
	return FALSE;
}