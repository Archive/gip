/*
 * GIP. A Software Manager Tool.
 *
 * Copyright (C) 1999, 2000 :
 * Andreas Hyden <andreas.hyden@telia.com>
 * and
 * Marmorat Irwin <mirwinm@yahoo.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
*/
 
/////////////////////////////////////////////////////////////////////////////
///////////////////// mainfunc.h - Main Functions ///////////////////////////
/////////////////////////////////////////////////////////////////////////////
 
#include <gnome.h>

#define src  0
#define bz2  1
#define tarZ 2
#define rpm  3
#define deb  4
#define zip  5
#define tar  6
#define non  20

struct fileinfo {
	gboolean configure;
	gboolean Makefile;
	gboolean Readme;
	gboolean FAQ;
} finfo;
	
guint ListSelected_selected_row;

gboolean
finishes_with							(gchar *str, gchar *end);

int
FileType							(gchar* file);

void 
init_variables			 				(void);
	
int
gip_question 							(char *msg);

int
FileType 							(char* file);

void
on_install_software1_activate					(GtkMenuItem     *menuitem,
                                        			 gpointer         user_data);

void
on_uninstall_software1_activate       				(GtkMenuItem     *menuitem,
                                        			 gpointer         user_data);
                                
void                                            	
on_file_type_converter1_activate				(GtkMenuItem	 *menuitem,
					 			 gpointer         user_data);
								 
void        
on_view_all_software_activate	 			  	(GtkMenuItem	 *menuitem,
								 gpointer         user_data);
void
on_view_uninstalled_software_activate				(GtkMenuItem	 *menuitem,
						 		 gpointer	  user_data);
void
on_view_gnome_softwaremap_activate				(GtkMenuItem	*menuitem,
						 		 gpointer	user_data);
void
on_exit1_activate 			                        (GtkMenuItem       *menuitem,
                                        			 gpointer           user_data);

void
on_preferences1_activate			   	 	(GtkMenuItem 		*menuitem,
								  gpointer			 user_data);

void        
on_user_guide1_activate				     		(GtkMenuItem	    *menuitem,
								  gpointer     	     user_data);

void
on_faq_activate							 (GtkMenuItem		*menutitem,
								  gpointer 		 	 user_data);

void
on_about1_activate           			                 (GtkMenuItem     	*menuitem,
                                         			  gpointer         	 user_data);
								 
void
UpdateGUI	   						 (void);
      
void
on_Preferences_cancel_activate			 		(GtkWidget	*widget,
								  gpointer	user_data);


void
on_MainWindow_show						(GtkWidget * widget,
								 gpointer 	user_data);

void
on_inst_list_select_row           (GtkCList        *clist,
                                  gint             row,
		                  gint             column,
		                  GdkEvent        *event,
		                  gpointer         user_data);