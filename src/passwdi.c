/*
 * GIP. A Software Manager Tool.
 *
 * Copyright (C) 1999, 2000 :
 * Andreas Hyden <andreas.hyden@telia.com>
 * and
 * Marmorat Irwin <mirwinm@yahoo.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
*/

/////////////////////////////////////////////////////////////////////////////
///////////// passwdi.c - Password Interface ////////////////////////////////
/////////////////////////////////////////////////////////////////////////////

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <gnome.h>
#include "passwdi.h"
#include "passwdfunc.h"
#include "support.h"

GnomeDialog*
create_Password_window ()
{
  GnomeDialog* Password_dialog;
  GtkWidget* label1;
  GtkWidget* Password_pixmap;
  GtkWidget* hbox;
  gchar* pixmap_filename;

  Password_dialog = gnome_dialog_new ("Enter root password", GNOME_STOCK_BUTTON_OK, GNOME_STOCK_BUTTON_CANCEL, NULL);
 
  label1 = gtk_label_new (_("Enter root Password:"));
  gtk_widget_set_usize (label1, 50, 16);
  gtk_label_set_justify (GTK_LABEL (label1), GTK_JUSTIFY_LEFT);
  gtk_label_set_line_wrap (GTK_LABEL (label1), TRUE);

  Password_pixmap = gtk_type_new (gnome_pixmap_get_type ());
  pixmap_filename = gnome_pixmap_file ("gnome-lockscreen.png");
  if (pixmap_filename)
    gnome_pixmap_load_file (GNOME_PIXMAP (Password_pixmap), pixmap_filename);
  else
    g_warning (_("Couldn't find pixmap file: %s"), "gip-uninstall.png");
  g_free (pixmap_filename);
  gtk_widget_set_usize (Password_pixmap, 56, 48);
 
  Password_entry = gtk_entry_new ();
  gtk_widget_set_usize (Password_entry, 304, 24);
  gtk_entry_set_visibility (GTK_ENTRY (Password_entry), FALSE);
  
  hbox = gtk_hbox_new (FALSE, 0);
  gtk_box_pack_start (GTK_BOX (hbox), Password_pixmap, TRUE, TRUE, 0);
  gtk_box_pack_start (GTK_BOX (hbox), label1, TRUE, TRUE, 0);
  
  gtk_box_pack_start (GTK_BOX (GNOME_DIALOG (Password_dialog)->vbox), hbox, TRUE, TRUE, 0);
  gtk_box_pack_start (GTK_BOX (GNOME_DIALOG (Password_dialog)->vbox), Password_entry, TRUE, TRUE, 0);
  
  gnome_dialog_button_connect (GNOME_DIALOG (Password_dialog), 0,
  							GTK_SIGNAL_FUNC (on_password_ok_activate), Password_dialog);
  gnome_dialog_button_connect (GNOME_DIALOG (Password_dialog), 1,
  							GTK_SIGNAL_FUNC (on_password_cancel_activate), Password_dialog);
  
  gtk_widget_show (label1);
  gtk_widget_show (Password_pixmap);
  gtk_widget_show (hbox);
  gtk_widget_show (Password_entry);

  return Password_dialog;
}
