/*
 * GIP. A Software Manager Tool.
 *
 * Copyright (C) 1999, 2000 :
 * Andreas Hyden <andreas.hyden@telia.com>
 * and
 * Marmorat Irwin <mirwinm@yahoo.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
*/
 
/////////////////////////////////////////////////////////////////////////////
///////////////////// mainfunc.c - Main Functions ///////////////////////////
/////////////////////////////////////////////////////////////////////////////

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include "mainfunc.h"
#include "maini.h"
#include "softinstfunc.h"
#include "softinsti.h"
#include "softuninstfunc.h"
#include "softuninsti.h"
#include "convertfunc.h"
#include "converti.h"
#include "support.h"
#include "database.h"

#include <gnome.h>
#include <time.h>

extern GtkWidget * clist1;
extern GtkWidget * clist2;
extern GtkWidget * clist3;

gboolean
finishes_with					(gchar *str, gchar *end)
{
	int len_str, len_end;
	len_str = strlen(str);
	len_end = strlen(end);
	if (len_str < len_end) return FALSE;	/* What would happen if the filename was too short? */
	
	return !strcmp(str + len_str - len_end, end);
}

int
FileType					(gchar* file)
{
	if ( finishes_with(file, ".tgz") || finishes_with(file, ".tar.gz") )
		return src;
	else if ( finishes_with(file, ".tar.bz2") )
		return bz2;
	else if ( finishes_with(file, ".tar.Z") )
		return tarZ;
	else if ( finishes_with(file, ".zip") )
		return zip;
	else if ( finishes_with(file, ".tar") )
		return tar;
	else if ( finishes_with(file, ".rpm") )
		return rpm;
	else
		return non;		
}

/* Initiates some variables */
void
init_variables		 			(void)
{
	storeglobal  = TRUE;
	instshown    = FALSE;
	uninstshown  = FALSE;
	convertshown = FALSE;	
}


/* Question */
int
gip_question 					(char *msg)
{
	GtkWidget *box;
        int val;
         
        box = gnome_message_box_new (msg, 
	        GNOME_MESSAGE_BOX_QUESTION,
	        GNOME_STOCK_BUTTON_YES,
                GNOME_STOCK_BUTTON_NO,
                NULL);
        val = gnome_dialog_run (GNOME_DIALOG (box));
	return val;
}

/* Install Software button pressed */
void
on_install_software1_activate          		(GtkMenuItem     *menuitem,
                                        	gpointer         user_data)
{
	GtkWidget*	installSoftware;
	
	if (instshown == FALSE)
	{
		instshown = TRUE;
		installSoftware = create_InstallSoftware_window ();
		gtk_widget_show (installSoftware);
	}
	else
		return;	
}

/* Uninstall Software button pressed */
void
on_uninstall_software1_activate       		(GtkMenuItem     *menuitem,
                                       		 gpointer         user_data)
{
	GtkWidget* Uninstall;
	gchar* tempfile[1];
	if (uninstshown == FALSE)
	{
		uninstshown = TRUE;
		uninst_selected_row = -1;
		Uninstall = create_Uninstall_window ();
		
		if (ListSelected_selected_row != -1) {
  			tempfile[0] = g_strdup (DataBase_Get_Name_From_Entry_Num (ListSelected_selected_row));
  			gtk_clist_append (GTK_CLIST (PackageUninstall_clist), tempfile);
	 	}
		
		gtk_widget_show (Uninstall);
	}
	else
		return;
}

/* File type convert menu item pressed */
void        
on_file_type_converter1_activate	   	(GtkMenuItem	    *menuitem,
					   	 gpointer        user_data)
{
	GtkWidget* convert;
	if (convertshown == FALSE)
	{
		convertshown = TRUE;
		convert = create_Convert_window ();
		gtk_widget_show (convert);
	}
	else
		return;
}

void        
on_view_all_software_activate	 	  	(GtkMenuItem	*menuitem,
						gpointer        user_data)
{
	gtk_notebook_set_page (GTK_NOTEBOOK (GIP_notebook), 0);
}

void
on_view_uninstalled_software_activate		(GtkMenuItem	*menuitem,
						 gpointer	user_data)
{
	gtk_notebook_set_page (GTK_NOTEBOOK (GIP_notebook), 1);
}

void
on_view_gnome_softwaremap_activate		(GtkMenuItem	*menuitem,
						 gpointer	user_data)
{
	gtk_notebook_set_page (GTK_NOTEBOOK (GIP_notebook), 2);
}							
							

/* Userguide menu item pressed */
void        
on_user_guide1_activate				 (GtkMenuItem	    *menuitem,
						  gpointer        user_data)
{
	/* FIXME: Use $PREFIX from config.h */
	system ("gnome-help-browser file:/usr/doc/gip/Userguide &");
}								 

/* FAQ menu item pressed */
void
on_faq_activate					(GtkMenuItem	*menutitem,
						gpointer 	 user_data)
{
	/* FIXME: Use $PREFIX from config.h */
	system ("gnome-help-browser file:/usr/doc/gip/FAQ &");
}


/* Exit */
void
on_exit1_activate                     	 	(GtkMenuItem     *menuitem,
                                       		 gpointer         user_data)
{                                       	
	gtk_main_quit();
}

/* Preferences */
void
on_preferences1_activate			(GtkMenuItem 	*menuitem,
					 	gpointer		 user_data)
{
/*
	GtkWidget* pref;
	if (onewindow == TRUE)
		return;
	pref = create_Preferences_window ();
	gtk_widget_show (pref);
	onewindow = TRUE;
*/
}

/* About GIP */
void
on_about1_activate                     		(GtkMenuItem     *menuitem,
                                        	gpointer         user_data)
{
	GtkWidget* About = create_About ();
	gtk_widget_show (About);
}
								 
/* Preferences (not yet implented) */
void
on_Preferences_cancel_activate			(GtkWidget		*widget,
						gpointer		 user_data)
{
	/*
	gtk_widget_destroy (GTK_WIDGET (Preferences_window));
	onewindow = FALSE;
	*/
}

/* Updates the GUI */
void
UpdateGUI					(void)
{
	while (gtk_events_pending ())
		gtk_main_iteration ();
}

void
on_MainWindow_show(GtkWidget * widget, gpointer user_data)
{
    DataBase_UpdateAllView();
}

void
on_inst_list_select_row            		(GtkCList        *clist,
                                 		 gint             row,
		                 		 gint             column,
		                 		 GdkEvent        *event,
		                 		 gpointer         user_data)
{                                      	
  ListSelected_selected_row = row;
  DataBase_Util_Show_Info(row);
}